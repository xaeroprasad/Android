package com.example.trailproject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;







import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

public class AndroidPermissionCollection extends ActionBarActivity {
	HashMap<String,ArrayList<String>> AndroidPermissionMultimap=new HashMap<String,ArrayList<String>>();	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_app_permission);
	}
	public HashMap<String,ArrayList<String>> AndroidpermissionsCollectionList(){
		AndroidPermissionMultimap.put("android.permission.ACCESS_NETWORK_STATE", new ArrayList<String>(Arrays.asList("Network Connection","View Network Connection","Allows applications to access information about networks")));
		AndroidPermissionMultimap.put("com.android.vending.BILLING", new ArrayList<String>(Arrays.asList("Network Connection","View Network Connection","Allows user to purchase items from google play within the application")));
		AndroidPermissionMultimap.put("android.permission.VIBRATE", new ArrayList<String>(Arrays.asList("VIBRATE","Affects Battery","Allows access to the vibrator")));
		AndroidPermissionMultimap.put("android.permission.WRITE_EXTERNAL_STORAGE", new ArrayList<String>(Arrays.asList("Storage","Modify or delect content of the USB storage","Allows the app to write to the USB storage")));
		AndroidPermissionMultimap.put("android.permission.READ_EXTERNAL_STORAGE", new ArrayList<String>(Arrays.asList("Storage","Modify or delect content of the USB storage","Allows the app to read to the USB storage")));
		
		return AndroidPermissionMultimap;
	}
}
