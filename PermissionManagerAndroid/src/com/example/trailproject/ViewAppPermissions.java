package com.example.trailproject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ViewAppPermissions extends ActionBarActivity {
	ListView List_Permissions;
	int SelectedListItemNo;
	int ElementExists=0,CatagoryExist=0;
	ArrayList<String> keys=new ArrayList<String>();
	ArrayList<String> AddCheckCatagoryList=new ArrayList<String>();
	ArrayList<String> myKey=new ArrayList<String>();
	ArrayList<String> addallArrayList=new ArrayList<String>();
	ArrayList PermissionList=new ArrayList();
	ArrayList PermissionListCopy=new ArrayList();
	ArrayList NopermissionMatchList=new ArrayList();
	ArrayList<String> GetListFromHashMap=new ArrayList<String>();
	HashMap<Integer,ArrayList<String>> Multimap=new HashMap<Integer,ArrayList<String>>();	
	HashMap<String,ArrayList<String>> GetAndPerMap=new HashMap<String,ArrayList<String>>();	
	LinkedHashMap<String,ArrayList<String>> GetAndPerMap1=new LinkedHashMap<String,ArrayList<String>>();	
	HashMap<String,ArrayList<String>> AndroidPermissionMultimap=new HashMap<String,ArrayList<String>>();	
	HashMap<String ,HashMap<String ,ArrayList<String>>> HashInHash=new HashMap<String, HashMap<String ,ArrayList<String>>>();
	LinkedHashMap<String,ArrayList<String>> sortedMap=new LinkedHashMap<String,ArrayList<String>>();	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_app_permission);
		List_Permissions=(ListView)findViewById(R.id.List_Permissions);
		List_Permissions.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);   
		List_Permissions.setItemsCanFocus(false);
		Bundle bundle = this.getIntent().getExtras();
		Multimap=(HashMap<Integer, ArrayList<String>>) bundle.getSerializable("HashMap");
		SelectedListItemNo=bundle.getInt("SelectedListItemNo");
		AndroidPermissionCollection GetListData=new AndroidPermissionCollection();
		AndroidPermissionMultimap=GetListData.AndroidpermissionsCollectionList();
		
//------------------------------Listing permission in order for each application---------------------------		
		for(int count=0;count<Multimap.get(SelectedListItemNo).size();count++){
			PermissionListCopy.add(Multimap.get(SelectedListItemNo).get(count));
		}		
//-----------------------------Selecting permission from Android permission class----------------------------
		for(int count=0;count<PermissionListCopy.size();count++){
			if(AndroidPermissionMultimap.get(PermissionListCopy.get(count))!=null){
				GetAndPerMap.put((String) PermissionListCopy.get(count), AndroidPermissionMultimap.get(PermissionListCopy.get(count)));		
			     }else{
			    	 NopermissionMatchList.add(PermissionListCopy.get(count));
			     }
			}
//---------------------------------------------------------------------------------------------------------------		
		PermissionList.addAll(GetAndPerMap.keySet());
//--------------------------------------Sorting permissions------------------------------------------------------
		if(GetAndPerMap.size()!=0){
		for(int count=0;count<PermissionList.size();count++){
			if(count==0){
				sortedMap.put((String) PermissionList.get(count), GetAndPerMap.get(PermissionList.get(count)));
				addallArrayList.addAll(GetAndPerMap.get(PermissionList.get(count)));
				AddCheckCatagoryList.add(addallArrayList.get(0));
				Log.d("ff", addallArrayList.get(0));
				Log.d("ff", addallArrayList.get(1));
				Log.d("ff", addallArrayList.get(2));
				addallArrayList.clear();
			}else{
				for(int check=0;check<AddCheckCatagoryList.size();check++){
					if(GetAndPerMap.get(PermissionList.get(count)).get(0).equals(AddCheckCatagoryList.get(check))){
						CatagoryExist=1;
					}
				} if(CatagoryExist!=1){
					addallArrayList.clear();
					sortedMap.put((String) PermissionList.get(count), GetAndPerMap.get(PermissionList.get(count)));
					AddCheckCatagoryList.add(GetAndPerMap.get(PermissionList.get(count)).get(0));
				         }
					if(CatagoryExist!=1){
						//sortedMap.put((String) PermissionList.get(count), GetAndPerMap.get(PermissionList.get(count)));
						addallArrayList.addAll(GetAndPerMap.get(PermissionList.get(count)));
						//AddCheckCatagoryList.add(addallArrayList.get(0));
						Log.d("ff", addallArrayList.get(0));
						Log.d("ff", addallArrayList.get(1));
						Log.d("ff", addallArrayList.get(2));
						addallArrayList.clear();
					}
				
			}
		 if(CatagoryExist!=1){	
		 for(int count2=count+1;count2<PermissionList.size();count2++){
				if(GetAndPerMap.get(PermissionList.get(count)).get(0).equals(GetAndPerMap.get(PermissionList.get(count2)).get(0))){
					sortedMap.put((String) PermissionList.get(count2), GetAndPerMap.get(PermissionList.get(count2)));
					addallArrayList.addAll(GetAndPerMap.get(PermissionList.get(count2)));
				}
			}	
		}
		 CatagoryExist=0;
			Log.d("end", "----------------------");
		  } 
		myKey.addAll(sortedMap.keySet());
	}
		else{
			sortedMap.put("0", NopermissionMatchList);
		}
		
		
		Listadapter lstadapter1=new Listadapter(ViewAppPermissions.this,sortedMap,myKey);
		List_Permissions.setAdapter(lstadapter1);
	}
	class Listadapter extends BaseAdapter{
		Context context;
		ArrayList<String>myKey;
		LinkedHashMap<String,ArrayList<String>> sortedMap;	
		//ArrayList PermissionList;
			public Listadapter(Context context,LinkedHashMap sortedMap,ArrayList<String>myKey){
				this.context=context;
				this.sortedMap=sortedMap;
				this.myKey=myKey;
			}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return sortedMap.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			LayoutInflater inflator=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			View row=inflator.inflate(R.layout.list_permission_mapping,arg2,false);
			TextView CatagoryTitle=(TextView)row.findViewById(R.id.CatagoryTitle);
			TextView CatagoryName=(TextView)row.findViewById(R.id.CatagoryName);
			TextView CatagoryDetails=(TextView)row.findViewById(R.id.CatagoryDetail);
			CatagoryTitle.setText(sortedMap.get(myKey.get(arg0)).get(0));
			CatagoryName.setText(sortedMap.get(myKey.get(arg0)).get(1));
			CatagoryDetails.setText(sortedMap.get(myKey.get(arg0)).get(2));
			//PermissionText.setText(PermissionList.get(arg0).toString());
			return row;
		}
		
	}
}

/*
AndroidPermissionCollection GetListData=new AndroidPermissionCollection();
AndroidPermissionMultimap=GetListData.AndroidpermissionsCollectionList();
for(int count=0;count<PermissionList.size();count++){

	for(int ArrayListCount=0;ArrayListCount<AndroidPermissionMultimap.get(PermissionList.get(count)).size();ArrayListCount++){
		GetListFromHashMap.add(AndroidPermissionMultimap.get(PermissionList.get(count)).get(ArrayListCount));	
	} 
	GetSelectedAppPermissionMultimap.put((String) PermissionList.get(count),new ArrayList<String>(GetListFromHashMap));
	GetListFromHashMap.clear();
}

keys.addAll(GetSelectedAppPermissionMultimap.keySet());
for(int count=0;count<keys.size();count++){
	GetCatagoryFromGetSelectedAppPermissionMultimap.add(GetSelectedAppPermissionMultimap.get(keys.get(count)).get(0));
}

for(int count=0;count<GetCatagoryFromGetSelectedAppPermissionMultimap.size();count++){
	for(int ParentLoopcount=0;ParentLoopcount<AddCheckCatagoryList.size();ParentLoopcount++){
		if(AddCheckCatagoryList.size()!=0 && GetCatagoryFromGetSelectedAppPermissionMultimap.get(count).equals(AddCheckCatagoryList.get(ParentLoopcount))){
			CatagoryExist=1;
		    }
	     }
	
	if(count==0 || CatagoryExist!=1  ){
	for(int count2=0;count2<GetCatagoryFromGetSelectedAppPermissionMultimap.size();count2++){	
		if(count2==0 || CatagoryExist!=1  )
			Log.d("fff",(String) GetCatagoryFromGetSelectedAppPermissionMultimap.get(count) );
		if(count2==0 || GetCatagoryFromGetSelectedAppPermissionMultimap.get(count).equals(GetCatagoryFromGetSelectedAppPermissionMultimap.get(count2)) ){
			if(AddCheckCatagoryList.size()==0){
				AddCheckCatagoryList.add((String) GetCatagoryFromGetSelectedAppPermissionMultimap.get(count));	
			}else{
			int size=AddCheckCatagoryList.size();
			for(int checkcount=0;checkcount<size;checkcount++){			
					if(checkcount==0 && AddCheckCatagoryList.size()==0){
						AddCheckCatagoryList.add((String) GetCatagoryFromGetSelectedAppPermissionMultimap.get(count));
						
						
					}else if(count==0){
						
					}else if(GetCatagoryFromGetSelectedAppPermissionMultimap.get(count).equals(AddCheckCatagoryList.get(checkcount))&& count!=0){
						ElementExists=1;
					}else if(checkcount==AddCheckCatagoryList.size()-1 && ElementExists!=1 ){
						AddCheckCatagoryList.add((String) GetCatagoryFromGetSelectedAppPermissionMultimap.get(count));	
				}		
			}
		}
			//AddCheckCatagoryList.add((String) GetCatagoryFromGetSelectedAppPermissionMultimap.get(count));
			addallArrayList.addAll(GetSelectedAppPermissionMultimap.get(keys.get(count)));
			hash.put(keys.get(count2), new ArrayList<String>(addallArrayList));
			addallArrayList.clear();
		}
	  }
	}
	/*for(int c=0;c<AddCheckCatagoryList.size();c++)
	Log.d("ff", AddCheckCatagoryList.get(c));
	Log.d("ff", "-------------------");
	HashInHash.put((String) GetCatagoryFromGetSelectedAppPermissionMultimap.get(count), hash);
}*/
