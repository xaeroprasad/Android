package com.example.trailproject;



import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;















import java.util.jar.JarFile;















import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AppDisplayLayout extends ActionBarActivity {
	Context context;

	Button UserApp,SystemApp,OpenPermission,GetButtons;
	String Buttonclick="UserApp";
	AlertDialog.Builder ListPopUp;
	int SelectedListItemNo;
	ListView BindAppToList;
	ProgressDialog dialog;
	Listadapter lstAdapter;
	LinearLayout LinearMenuBar;
	RelativeLayout RelativeLayoutMenu;
	ArrayList<String> AppName = new ArrayList<String>();
	ArrayList<Drawable> AppImage = new ArrayList<Drawable>();
	ArrayList<String> SysAppName = new ArrayList<String>();
	ArrayList<Drawable> SysAppImage = new ArrayList<Drawable>();
	HashMap<Integer,ArrayList<String>> Multimap=new HashMap<Integer,ArrayList<String>>();	
	ArrayList PermissionList=new ArrayList();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_app_display_layout);
		UserApp=(Button)findViewById(R.id.UserApp);
		SystemApp=(Button)findViewById(R.id.SystemApp);
		BindAppToList=(ListView)findViewById(R.id.BindAppToList);
		LinearMenuBar=(LinearLayout)findViewById(R.id.LinearMenuBar);
		RelativeLayoutMenu=(RelativeLayout)findViewById(R.id.RelativeLayoutMenu);
		context=this;
		GradianLayout(50,RelativeLayoutMenu,"#FFFFFF");
		GradianTextview(200,UserApp,"#FE2E64");
		GradianTextview(200,SystemApp,"#FFFFFF");

		//GetListValues();
		DummyAsyncTask task = new DummyAsyncTask();
		task.execute();
		
//------------------------Listener UserApp--------------------------------------------------
		UserApp.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Handler handler=new Handler();
				handler.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						UserApp.getBackground().setAlpha(500);
						BindAppToList.setAdapter(null);	
						
						//Toast.makeText(getApplicationContext(), "text", Toast.LENGTH_SHORT).show();
						Buttonclick="UserApp";
						DummyAsyncTask task = new DummyAsyncTask();
						task.execute();
						
					}
				},300);
				UserApp.getBackground().setAlpha(100);
			}
		});
		
//------------------------Listener SystemApp--------------------------------------------------
		SystemApp.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Handler handler=new Handler();
				handler.postDelayed(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						SystemApp.getBackground().setAlpha(500);
						
						BindAppToList.setAdapter(null);
						Buttonclick="SystemApp";
						DummyAsyncTask task = new DummyAsyncTask();
						task.execute();
					}
					
				},200);
				SystemApp.getBackground().setAlpha(100);
			}
		});
	}

	
//----------------------Functions---------------------------------------------------	
	void GetListValues(){
		Handler handler=new Handler();
		handler.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				dialog.dismiss();	
				PackageManager pm = getPackageManager();
				List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
				for(ApplicationInfo packageInfo:packages){
				    if( pm.getLaunchIntentForPackage(packageInfo.packageName) != null ){
				                String currAppName = pm.getApplicationLabel(packageInfo).toString();
				                AppName.add(pm.getApplicationLabel(packageInfo).toString());
				                AppImage.add(getApplicationContext().getPackageManager().getApplicationIcon(packageInfo));
				                // Log.d("TAG", "Installed package :" + pm.getApplicationLabel(packageInfo).toString());
				                //Drawable icon;
				                //icon = getApplicationContext().getPackageManager().getApplicationIcon(packageInfo);
				                
				                //This app is a non-system app
				    }
				    else{
				        //System App
				    }
				    lstAdapter=new Listadapter(AppDisplayLayout.this,AppImage,AppName);
				    BindAppToList.setAdapter(lstAdapter);
				    
//-----------------------OnItemListListener--------------------------------------------	
				    
				    BindAppToList.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
								long arg3) {
						
							Toast.makeText(getApplicationContext(), "Hello",Toast.LENGTH_SHORT).show();
							
						}
					});
				    
				    
			  }
			}
		},2000);
		dialog = new ProgressDialog(context);
		dialog.setCancelable(false);
		dialog.setTitle("Loading....");
		dialog.setMessage("Searching");
		dialog.show();
	}
	
//-------------------------ListAdapter-----------------------------------------------
	class Listadapter extends BaseAdapter{
		Context context;
		ArrayList<Drawable> AppImage=new ArrayList<Drawable>();
		ArrayList<String> AppName=new ArrayList<String>();
		
			public Listadapter(Context c,ArrayList<Drawable> AppImage, ArrayList<String> AppName){
				this.context=c;
				this.AppImage=AppImage;
				this.AppName=AppName;
			}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return AppImage.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflator=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			View row=inflator.inflate(R.layout.listview_displaylayout,parent,false);
			ImageView AppImage1=(ImageView)row.findViewById(R.id.AppImage);
			TextView AppName1=(TextView)row.findViewById(R.id.AppName);
			AppImage1.setImageDrawable(AppImage.get(position));
			AppName1.setText(AppName.get(position));
			return row;
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.app_display_layout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
//--------------------------AsyncTask------------------------------------------------
	
	public class DummyAsyncTask extends AsyncTask<String, Void, Void>
	{
		ProgressDialog dialog;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(context);
			dialog.setCancelable(false);
			/*Handler handler=new Handler();
			handler.postDelayed(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					
				}
				
			},2000);*/
			dialog.setTitle("Loading....");
			dialog.setMessage("Searching");
			dialog.show();
			
		}
		protected Void doInBackground(String... arg0) {
				PackageManager pm = getPackageManager();
			
			//ApplicationInfo info;
			/*PackageInfo packageInfo1;
			try {
				packageInfo1 = pm.getPackageInfo(getPackageName(),0);
				//Log.d("d", packageInfo1.activities.toString());
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			
			//File manifest = new File(pkg.applicationInfo.publicSourceDir + "/AndroidManifest.xml");
			
			// filePath = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/Download/");
			//File[] listOfFiles = filePath.listFiles();

		   // for (int i = 0; i < listOfFiles.length; i++) {
		   // 	Log.d("sdf",listOfFiles[i].getName());
		    //}
			//Log.d("sdf",Path1.toString());
			//File pa= new File(Environment.getExternalStorageDirectory().getAbsolutePath());
			
			
			//File filePath = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
	            //Toast.makeText(getApplicationContext(),pa , Toast.LENGTH_SHORT).show();
			 //Log.d("Path",pa.toString());
			 
			 List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
			int multilistcount=0;
			 for(ApplicationInfo packageInfo:packages){
			    if( pm.getLaunchIntentForPackage(packageInfo.packageName) != null ){
			                String currAppName = pm.getApplicationLabel(packageInfo).toString();
			                AppName.add(pm.getApplicationLabel(packageInfo).toString());
			                AppImage.add(getApplicationContext().getPackageManager().getApplicationIcon(packageInfo));
			                String Path=packageInfo.packageName.toString();
			                try {
								PackageInfo packageInfo2 = pm.getPackageInfo(packageInfo.packageName, PackageManager.GET_PERMISSIONS);
								String[] requestedPermissions = packageInfo2.requestedPermissions;

							      if(requestedPermissions != null) {
							         for (int i = 0; i < requestedPermissions.length; i++) {
							           // Log.d(Path, requestedPermissions[i]);
							            PermissionList.add(requestedPermissions[i]);					          
							         }
							         Multimap.put(multilistcount, new ArrayList<String>(PermissionList));
							       //  Log.d("test", "--------------------------------");
							         
//------------------------------------------------------------------------------------------------------------------------------							        
							         try {
							             String installer = context.getPackageManager()
							                                         .getInstallerPackageName(context.getPackageName());
							             Log.d("tag", installer);
							         } catch (Throwable e) {          
							         }
							         String appPackageName=packageInfo.packageName;
							         Uri a=Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName);
							      String aa=a.toString();
							        
							      }
							      else{
							    	  PermissionList.add("No Permission Access");
							    	  Multimap.put(multilistcount, new ArrayList<String>(PermissionList));
									    
							      }
			                } catch (NameNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} 
			                PermissionList.clear();
			                multilistcount++;
			    }
			    else{
			        //System App
			    	String currAppName = pm.getApplicationLabel(packageInfo).toString();
	                SysAppName.add(pm.getApplicationLabel(packageInfo).toString());
	                SysAppImage.add(getApplicationContext().getPackageManager().getApplicationIcon(packageInfo));        
			    }
			    
			
		   }
			return null;
		}
		 @Override
			protected void onPostExecute(Void result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				
				dialog.dismiss();
				if(Buttonclick.equalsIgnoreCase("UserApp")){
				lstAdapter=new Listadapter(AppDisplayLayout.this,AppImage,AppName);
				BindAppToList.setAdapter(lstAdapter);
//-----------------------OnItemListListener--------------------------------------------	
			    
			    BindAppToList.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
							long arg3) {
						SelectedListItemNo=arg2;
						LayoutInflater inflater = getLayoutInflater();
						View ListPopUpWindow = inflater.inflate(R.layout.list_popup_window, null);
						ListPopUp=new AlertDialog.Builder(AppDisplayLayout.this);
						final AlertDialog alert = ListPopUp.create();
						alert.setView(ListPopUpWindow);
						alert.setCancelable(true);   
						//alert.show().getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
						alert.show();
						OpenPermission=(Button)ListPopUpWindow.findViewById(R.id.OpenPermissions);			    
					    OpenPermission.setOnClickListener(new View.OnClickListener() {
							
							@Override
							public void onClick(View v) {
								alert.dismiss();
								Handlercall(OpenPermission);
								Intent intent=new Intent(AppDisplayLayout.this,ViewAppPermissions.class);
								Bundle extras = new Bundle();
								extras.putSerializable("HashMap",Multimap);
								intent.putExtras(extras);		
								intent.putExtra("SelectedListItemNo", SelectedListItemNo);
								startActivity(intent);
								
							}
						});
					}
				});
				}
				//lstAdapter.notifyDataSetChanged();
				
				else{
				lstAdapter=new Listadapter(AppDisplayLayout.this,SysAppImage,SysAppName);
			    BindAppToList.setAdapter(lstAdapter);
			    //lstAdapter.notifyDataSetChanged();
				}
			    BindAppToList.setBackgroundColor(Color.BLACK);
			
		 }
		
	}
	void GradianLayout(int num,RelativeLayout name,String color)
	{
		GradientDrawable gd = new GradientDrawable();
	    gd.setColor(Color.parseColor(color));
	    gd.setCornerRadius(num);
	   // name.setBackgroundDrawable(gd);
	}
	void GradianTextview(int num,Button name,String color)
	{
		GradientDrawable gd = new GradientDrawable();
	    gd.setColor(Color.parseColor(color));
	    gd.setCornerRadius(num);
	   // name.setBackgroundDrawable(gd);
	}
	void Handlercall(Button GetButton){
		GetButtons=GetButton;
		Handler handler=new Handler();
		handler.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				GetButtons.getBackground().setAlpha(500);
				
			}
		}, 200);
		GetButtons.getBackground().setAlpha(200);
	}
	
}
