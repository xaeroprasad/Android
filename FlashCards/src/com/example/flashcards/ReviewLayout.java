package com.example.flashcards;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

//import com.example.testproject.R;


//import com.example.myfirsstpro.R;











import java.util.Random;

import com.google.api.translate.Language;
import com.google.api.translate.Translate;

import android.R.integer;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Path.FillType;
import android.graphics.PorterDuff.Mode;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.speech.tts.TextToSpeech;
import android.text.Html;

public class ReviewLayout extends ActionBarActivity implements
		TextToSpeech.OnInitListener {
	int flashcardcount, flashcardcount2,readcnt=0,colorchange=0, progresstextcount = 1,ParentHeight,
			Progeressfirstcount=0,ParentWidth,progressCount = 0, maragin=20,width,Height,
			ImadeId=10000,PictureId=10001,ImageChangeId,getfirstwordcount=0,WordImagwWidth=0;
	private TextToSpeech textToSpeech;
	Context context;
	boolean EnglishBtnPressed=false;
	File ConcatPathandPicName;
	File[] files;
	File[] Alphbetsfile;
	ContextWrapper Dircw;
	char[] PictureNameToChar;
	Button BtnNext, SelectBtn,BtnPrev,flip;
	String[] separated1;
	String flipchange="flipon",TamilState="pressed",HideEngLetter="false";
	ImageView WordImage,PictureImage,read,readwordbyword;
	int[] array;
	ArrayList<String> PictureName = new ArrayList<String>();
	ArrayList<Integer> WordID = new ArrayList<Integer>();
	ArrayList<Integer> RandomImageAndWordArray = new ArrayList<Integer>();
	ArrayList<String> ArrayAlphabetName = new ArrayList<String>();
	ArrayList<Integer> ShuffleingLetterArrayInt = new ArrayList<Integer>();
	ArrayList<String> ShuffleingLetterArray = new ArrayList<String>();
	ArrayList<String> ArrayanimalsName = new ArrayList<String>();
	ArrayList<File> AnimalsPictureFilePath = new ArrayList<File>();
	ArrayList<String> AlphbatNames = new ArrayList<String>();
	ArrayList<File> wordpathArray = new ArrayList<File>();
	ProgressBar progressBar;
	HashMap<String,Object[]> CopyMultimap;
	TextView ProgressTxt,TamilWord,FlipTxt;
	RelativeLayout Head,LettersLayout;
	LinearLayout parent,Body;
	int animalsImage,LayoutBackground,RadomLetterInt;
	RelativeLayout.LayoutParams para;
	Typeface tf;
	TamilHashMap GetHashMap=new TamilHashMap();
	int[] animalscollectiononclick,animalOrginal,animalscollection,animalsvoice,
	alphabetsimages,AnimalTamilVoice;
	String[] AnmialTamilName;
	Random Randomflashcardcount,RandomLetter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.learn_layout);
	    tf = Typeface.createFromAsset(getAssets(),
			     "fonts/Bamini.ttf");
	    Body=(LinearLayout)findViewById(R.id.Body);
		LinearLayout parent=(LinearLayout)findViewById(R.id.parent);
		textToSpeech = new TextToSpeech(this, this);
		readwordbyword=(ImageView)findViewById(R.id.readwordbyword);
		read = (ImageView) findViewById(R.id.read);
		BtnNext = (Button) findViewById(R.id.next);
		BtnPrev = (Button) findViewById(R.id.previous);
		ProgressTxt = (TextView) findViewById(R.id.ProgressTxt);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		Head= (RelativeLayout)findViewById(R.id.Head);
		LettersLayout= (RelativeLayout)findViewById(R.id.LettersLayout);
		flip=(Button)findViewById(R.id.flip);
		FlipTxt=(TextView)findViewById(R.id.FlipTxt);
		TamilHashMap GetHashMap=new TamilHashMap();
		Randomflashcardcount =new Random();
		RandomLetter=new Random();
		final ArrayList<HashMap> GetHashMapCollection=GetHashMap.HashMap();
		//HashMap<String,Object[]> CopyMultimap;
		Intent intent=new Intent();
		Bundle extra=getIntent().getExtras();
		int Listrow=extra.getInt("ListValue");
		if(Listrow==0){
		 CopyMultimap=GetHashMapCollection.get(0);
		 LayoutBackground=R.drawable.numbers_backgroung;
		}else if(Listrow==2){
		 CopyMultimap=GetHashMapCollection.get(1);
		 LayoutBackground=R.drawable.jungle;
		}else if(Listrow==3){
			 CopyMultimap=GetHashMapCollection.get(2);
			 LayoutBackground=R.drawable.family_background;
			}
		
	//-----------------Assiging values to layout----------------------------------
		animalscollectiononclick=new int[CopyMultimap.get("0").length];
		animalOrginal=new int[CopyMultimap.get("1").length];
		animalscollection=new int[CopyMultimap.get("2").length];
		animalsvoice=new int[CopyMultimap.get("3").length];
		alphabetsimages=new int[CopyMultimap.get("4").length];
		AnmialTamilName=new String[CopyMultimap.get("5").length];
		AnimalTamilVoice=new int[CopyMultimap.get("6").length];
		for(int cnt=0;cnt<CopyMultimap.get("0").length;cnt++){
			Object[] GetObjectArray=CopyMultimap.get("0");
			animalscollectiononclick[cnt]= Integer.parseInt(GetObjectArray[cnt].toString());
		}for(int cnt=0;cnt<CopyMultimap.get("1").length;cnt++){
			Object[] GetObjectArray=CopyMultimap.get("1");
			animalOrginal[cnt]= Integer.parseInt(GetObjectArray[cnt].toString());
		}for(int cnt=0;cnt<CopyMultimap.get("2").length;cnt++){
			Object[] GetObjectArray=CopyMultimap.get("2");
			animalscollection[cnt]= Integer.parseInt(GetObjectArray[cnt].toString());
		}for(int cnt=0;cnt<CopyMultimap.get("3").length;cnt++){
			Object[] GetObjectArray=CopyMultimap.get("3");
			animalsvoice[cnt]= Integer.parseInt(GetObjectArray[cnt].toString());
		}for(int cnt=0;cnt<CopyMultimap.get("4").length;cnt++){
			Object[] GetObjectArray=CopyMultimap.get("4");
			alphabetsimages[cnt]= Integer.parseInt(GetObjectArray[cnt].toString());
		}for(int cnt=0;cnt<CopyMultimap.get("5").length;cnt++){
			Object[] GetObjectArray=CopyMultimap.get("5");
			AnmialTamilName[cnt]= (GetObjectArray[cnt].toString());
		}for(int cnt=0;cnt<CopyMultimap.get("6").length;cnt++){
			Object[] GetObjectArray=CopyMultimap.get("6");
			AnimalTamilVoice[cnt]= Integer.parseInt(GetObjectArray[cnt].toString());
		}
	//-----------------Storing Names and Alphabet Names--------------------------
		//animalscollectiononclick=CopyMultimap.get("0");
		//Body.setBackgroundColor(Color.GREEN);
	
			try {
				
				for (int j=0;j<animalscollection.length;j++){
					ArrayanimalsName.add(this.getResources().getResourceEntryName(animalscollection[j]));
				}
				for (int i=0;i<alphabetsimages.length;i++){
					AlphbatNames.add(this.getResources().getResourceEntryName(alphabetsimages[i]));
				}
			}catch(Exception e){		
		}
			flashcardcount=Randomflashcardcount.nextInt(ArrayanimalsName.size()-1);
			RandomImageAndWordArray.add(flashcardcount);
	//-------------------------------------------------------------------------------------	
		progressBar.setProgress(1);
	}
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		ParentHeight=Head.getHeight();
		ParentWidth=Head.getWidth();
		RelativeLayout.LayoutParams para;
		RelativeLayout.LayoutParams ProgressBarPara;
		LinearLayout.LayoutParams Readbtnpara;
		int progressbarHeight=progressBar.getHeight();
		//para=new RelativeLayout.LayoutParams(BtnNext.getWidth(),BtnNext.getHeight()/2);
		para=new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.FILL_PARENT);
		para.addRule(RelativeLayout.ABOVE,R.id.progressBar);
		para.addRule(RelativeLayout.ALIGN_RIGHT,R.id.progressBar);
		flip.setLayoutParams(para);
		ProgressBarPara=new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,BtnNext.getHeight()/2);
		ProgressBarPara.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		Readbtnpara=new LinearLayout.LayoutParams(BtnNext.getWidth(),LinearLayout.LayoutParams.FILL_PARENT);
		Readbtnpara.topMargin=20;
		readwordbyword.setLayoutParams(Readbtnpara);
		read.setLayoutParams(Readbtnpara);
		progressBar.setPadding(5, 5, 5, 5);
		progressBar.setLayoutParams(ProgressBarPara);
		NextFlashCard(flashcardcount);
		ProgressTxt.setTextSize(10);
		FlipTxt.setTextSize(10);
		
	}

	@Override
	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			int result = textToSpeech.setLanguage(Locale.US);
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e("error", "This Language is not supported");
			}
		} else {
			Log.e("error", "Initilization Failed!");
		}

	}

	public void NextFlashCard(int count) {
		flashcardcount2 = count;
		read.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				TamilState="pressed";
				EnglishBtnPressed=true;
				flipchange="flipon";
				HideEngLetter="false";
				WordImagwWidth=0;
				Head.removeAllViews();
				getfirstwordcount=0;
				LettersLayout.removeAllViews();
				maragin=20;
				NextFlashCard(flashcardcount);
				Handler handler = new Handler();
				handler.postDelayed(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						read.getBackground().setAlpha(500);
						flip.setBackgroundResource(R.drawable.flip_icon);
						flip.getBackground().setAlpha(500);
					}
				}, 1000);
				read.getBackground().setAlpha(128);
				convertTextToSpeech(PictureName.get(0));
			}
		});

		BtnNext.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Head.removeAllViews();
				LettersLayout.removeAllViews();
				SelectBtn = BtnNext;
				flipchange="flipon";
				EnglishBtnPressed=false;
				WordImagwWidth=0;
				getfirstwordcount=0;
				HideEngLetter="false";
				ButtonColorChange();
				TamilState="pressed";
				PictureName.clear();
				maragin=20;
				ShuffleingLetterArray.clear();
				ShuffleingLetterArrayInt.clear();
				//flashcardcount++;
				flashcardcount=Randomflashcardcount.nextInt(ArrayanimalsName.size()-1);
				int Size=RandomImageAndWordArray.size();
				int loop = 0;
				for(loop=0;loop<Size;loop++){
					if(RandomImageAndWordArray.get(loop)==flashcardcount){
						flashcardcount=Randomflashcardcount.nextInt(ArrayanimalsName.size());	
					    loop=-1;
					}
				}
				RandomImageAndWordArray.add(flashcardcount);
				progresstextcount=RandomImageAndWordArray.size();
				flip.setBackgroundResource(R.drawable.flip_icon);
				flip.getBackground().setAlpha(500);
				//progressBar.setProgress(progressBar.getProgress() + 1);
				progressBar.setProgress(progresstextcount);
				NextFlashCard(flashcardcount);
				ProgressTxt.setText(progresstextcount + " of "+animalscollection.length);
				//progresstextcount++;
				ProgressTxt.setTypeface(null, Typeface.BOLD);
			}
		});
		BtnPrev.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Head.removeAllViews();
				LettersLayout.removeAllViews();
				maragin=20;
				EnglishBtnPressed=false;
				SelectBtn = BtnPrev;
				ShuffleingLetterArray.clear();
				ShuffleingLetterArrayInt.clear();
				WordImagwWidth=0;
				PictureName.clear();
				getfirstwordcount=0;
				flipchange="flipon";
				TamilState="pressed";
				HideEngLetter="false";
				ButtonColorChange();
				flashcardcount=RandomImageAndWordArray.get(progresstextcount-2);
				//flashcardcount--;
				progresstextcount--;
				flip.setBackgroundResource(R.drawable.flip_icon);
				flip.getBackground().setAlpha(500);
				NextFlashCard(flashcardcount);
				progressBar.setProgress(progresstextcount);
				//progressBar.setProgress(progressBar.getProgress() - 1);
				ProgressTxt.setText(progresstextcount + " of "+animalscollection.length);
			}
		});
		
	//-----------Initial Loading of Pictures and Images-------------------------
		
		try {
			String[] separated = ArrayanimalsName.get(flashcardcount2).split("\\.");
				PictureName.add(separated[0]);
				PictureNameToChar = separated[0].toCharArray();
				animalsImage=animalscollection[flashcardcount2];
				AnimalsPictureFilePath.add(ConcatPathandPicName);
				PictureImage = new ImageView(this);
				PictureImage.setId(PictureId++);
				PictureImage.setBackgroundResource(animalsImage);
				RelativeLayout.LayoutParams Picturepara=new RelativeLayout.LayoutParams(
						RelativeLayout.LayoutParams.WRAP_CONTENT,
						RelativeLayout.LayoutParams.WRAP_CONTENT);
				Picturepara.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
				Picturepara.addRule(RelativeLayout.ALIGN_PARENT_LEFT);		
				Picturepara.height=ParentHeight;
				Picturepara.width=ParentWidth;
				Picturepara.bottomMargin=40;
				Picturepara.leftMargin=31;
				
				PictureImage.setLayoutParams(Picturepara);
				PictureImage.setTag(PictureName.get(0));
				Head.addView(PictureImage);
				separated1=null;
				PictureImage.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						final View copyview=v;
						 Handler handler1=new Handler();
						 handler1.postDelayed(new Runnable(){
							 public void run() {
								 copyview.setBackgroundResource(animalsImage);
							 }
						 }, 2000);
								 copyview.setBackgroundResource(animalscollectiononclick[flashcardcount2]);
								 ModeSound(animalsvoice[flashcardcount2]);
					}
				});
				
			//-----------------------------Shuffling English Words ----------------------------------
				//char[] SelectRandomLetter=PictureNameToChar;
				if(EnglishBtnPressed!=true){
				int Size=PictureNameToChar.length;
				int loop = 0,loop1=0;
				RadomLetterInt=RandomLetter.nextInt(Size);
				ShuffleingLetterArray.add(String.valueOf(PictureNameToChar[RadomLetterInt]));
				ShuffleingLetterArrayInt.add(RadomLetterInt);
				for(loop1=0;loop1< Size-1;loop1++){
				for(loop=0;loop<ShuffleingLetterArrayInt.size();loop++){
					if(loop==0)
					RadomLetterInt=RandomLetter.nextInt(Size);
					if(ShuffleingLetterArrayInt.get(loop)==RadomLetterInt){		
						loop=-1;
					}
				}
				ShuffleingLetterArray.add(String.valueOf(PictureNameToChar[RadomLetterInt]));
				ShuffleingLetterArrayInt.add(RadomLetterInt);
				 }
				}
			//------------------------------------------------------------------------------
			
					for (int getcharcount = 0; getcharcount < PictureNameToChar.length; getcharcount++) {
					for (int getwordcount = 0; getwordcount < 26; getwordcount++) {
						String[] separated1 = AlphbatNames.get(getwordcount)
									.split("\\.");
						String getSplitString = separated1[0];
						if (getSplitString.equalsIgnoreCase(String
								.valueOf(ShuffleingLetterArray.get(getcharcount)))) {
							int i=0;
							WordImage = new ImageView(this);
							WordImage.setId(ImadeId--);				
							WordImage.setBackgroundResource(alphabetsimages[getwordcount]);
							RelativeLayout.LayoutParams para=new RelativeLayout.LayoutParams(
									RelativeLayout.LayoutParams.WRAP_CONTENT,
									RelativeLayout.LayoutParams.WRAP_CONTENT);
							para.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
							para.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
							if(getfirstwordcount!=0 && PictureNameToChar.length>=10 )
							para.leftMargin=WordImagwWidth+=41;
							else if(getfirstwordcount!=0 )
								para.leftMargin=WordImagwWidth+=66;
							else
							para.leftMargin=WordImagwWidth+=10;	
							para.bottomMargin=25;
							para.width=BtnNext.getWidth()/2;
							para.height=BtnNext.getHeight();
							WordImage.setLayoutParams(para);
							getfirstwordcount++;
							WordImage.setTag(getSplitString);
							Head.setBackgroundResource(LayoutBackground);
							//Button ShuffleLetter=new Button(ReviewLayout.this);
							/*ShuffleLetter.setBackgroundResource(R.drawable.alphabet_btn);
							ShuffleLetter.setLayoutParams(para);
							ShuffleLetter.setTag(getSplitString);
							ShuffleLetter.setText(getSplitString);*/
//							SelectRandomLetter;
//							PictureNameToChar;
							if(HideEngLetter.equalsIgnoreCase("false"))
								LettersLayout.addView(WordImage);
								//LettersLayout.addView(ShuffleLetter);
							final int cnt=PictureNameToChar.length;
							final char[] wordarray=PictureNameToChar;
							final String wordname=PictureName.get(0);
							
			//----------------------TamilMode------------------------------------------------				
			    readwordbyword.setOnClickListener(new OnClickListener() {
							int[] id;
								@Override
								public void onClick(View v) {	
									if(TamilState.equalsIgnoreCase("pressed")){
									TamilState="released ";
									//NextFlashCard(flashcardcount);
									WordImagwWidth=0;
									LettersLayout.removeAllViews();
									//convertTextToSpeech(AnimalTamilVoice[flashcardcount2]);
									ModeSound(AnimalTamilVoice[flashcardcount2]);
									Handler handler=new Handler();
									handler.postDelayed(new Runnable(){
										@Override
										public void run() {
											readwordbyword.getBackground().setAlpha(500);
											flip.setBackgroundResource(R.drawable.flip_icon);
											flip.getBackground().setAlpha(500);
											flipchange="flipon";
										}										
									},1000);
									readwordbyword.getBackground().setAlpha(128);
									readcnt=0;					
									ViewGroup vg = (ViewGroup) Head;
									int getchildcnt=vg.getChildCount();
									ArrayList<String> qwe=new ArrayList<String>();
									for(int i=1;i<getchildcnt;i++){
									View child = vg.getChildAt(i);
									String na=((String)child.getTag());
									int a= vg.getChildAt(i).getId();	
									WordID.add(a);
									}
									for(int i=0;i<WordID.size();i++){
										View child =vg.findViewById(WordID.get(i));
											Head.removeView(child);
									}
									TextView TamilWord=new TextView(ReviewLayout.this);
									TamilWord.setTypeface(tf);
									ArrayList<String> PictureNameToTamilChar = new ArrayList<String>();
									String a[]=AnmialTamilName[flashcardcount2].split("(?!^)");
									//ArrayList<char[]> PictureNameToTamilChar = new ArrayList<char[]>(Arrays.asList(AnmialTamilName[flashcardcount2].toCharArray()));
								    //PictureNameToTamilChar.add(AnmialTamilName[flashcardcount2].toCharArray());
									String Name="தமிழ்";
											char[] PictureNameToChar24 = Name.toCharArray();
											Log.v("hg", PictureNameToChar24.toString());
											System.out.print(PictureNameToChar24);
									for (int i = 0;i < AnmialTamilName[flashcardcount2].length(); i++){
										//char a=AnmialTamilName[flashcardcount2].charAt(i);
										PictureNameToTamilChar.add(String.valueOf(AnmialTamilName[flashcardcount2].charAt(i)));
									}
									TamilWord.setText(AnmialTamilName[flashcardcount2]);
									TamilWord.setHeight(100);
									RelativeLayout.LayoutParams para1=new RelativeLayout.LayoutParams(
											RelativeLayout.LayoutParams.WRAP_CONTENT,
											RelativeLayout.LayoutParams.WRAP_CONTENT);
									para1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,TamilWord.getId());
									para1.leftMargin=25;
									para1.topMargin=30;
									para1.bottomMargin=25;
									TamilWord.setLayoutParams(para1);
									TamilWord.setTextColor(Color.parseColor("#298A08"));
									TamilWord.setTextSize(16 * getResources().getDisplayMetrics().density);
									//TamilWord.setTextSize(BtnNext.getWidth()/4);
									TamilWord.setId(100);
									//Head.addView(TamilWord);
									LettersLayout.addView(TamilWord);
								}else{
									//convertTextToSpeech(AnimalTamilVoice[flashcardcount2]);
									ModeSound(AnimalTamilVoice[flashcardcount2]);
								}
			                  }
							});
					//--------------------------------------------------------------------
							WordImage.setOnClickListener(new OnClickListener() {	
							//ShuffleLetter.setOnClickListener(new OnClickListener() {	
								@Override
								public void onClick(View v) {
								final View copyview=v;
									Handler handler=new Handler();
									handler.postDelayed(new Runnable() {										
										@Override
										public void run() {
											copyview.getTag();
											for(int i=0;i<alphabetsimages.length;i++){
												if(copyview.getTag().toString().
														equalsIgnoreCase(ReviewLayout.this.getResources().
																getResourceEntryName(alphabetsimages[i]))){
													copyview.getBackground().setAlpha(500);
			
												}
											}
										}
									},500 );
									
									v.getBackground().setAlpha(150);
									convertTextToSpeech(v.getTag().toString()); 
									}
							});
							break;
						}
				}
			}
		 		
		} catch (Exception e) {
			
			Log.v("dd", e.toString());
		}
		PictureNameToChar=null;
		if(Progeressfirstcount==0){
		ProgressTxt.setText("1" + " of "+animalscollection.length);
		Progeressfirstcount++;
		}
		progressBar.setMax(animalscollection.length);
		flip.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {	
				RelativeLayout copylayout;
				copylayout=Head;
				if(flipchange.equalsIgnoreCase("flipon")){
                Head.removeAllViews();
                getfirstwordcount=0;
                WordImagwWidth=0;
                SelectBtn=flip;
                ButtonColorChange();
                flip.setBackgroundResource(R.drawable.flip_iconon);
                Head.setBackgroundResource(animalOrginal[flashcardcount]);
                flipchange="flipoff";
				}else {
					HideEngLetter="true";
					flip.setBackgroundResource(R.drawable.flip_icon);
					Head.removeAllViews();
					maragin=20;
					SelectBtn=flip;
					ButtonColorChange();
					NextFlashCard(flashcardcount);
					flipchange="flipon";
				}
			}
		});
	}

	public void convertTextToSpeech(String Name) {
		AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		int amStreamMusicMaxVol = am.getStreamMaxVolume(am.STREAM_MUSIC);
		am.setStreamVolume(am.STREAM_MUSIC, amStreamMusicMaxVol, 0);
		textToSpeech.setSpeechRate(0.7f);
		textToSpeech.speak(Name, TextToSpeech.QUEUE_FLUSH, null);
	}
	
	public void ButtonColorChange() {
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				SelectBtn.getBackground().setAlpha(500);
			}
		}, 200);	
		//SelectBtn.setBackgroundColor(Color.BLACK);
		SelectBtn.getBackground().setAlpha(128);
	}
	public void ModeSound(int VoiceID){
		final MediaPlayer mp = MediaPlayer.create(this, VoiceID);
		mp.start();
	}
	public static void main(String[] args) {
		
	}
}
