package com.example.flashcards;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class CanvasDraw extends View{
Paint paint=new Paint();
Path  path=new Path();
Paint CirclePaint=new Paint();
Path CirclePath=new Path();
LinearLayout parent;
LayoutParams para;
Button reset;
	public CanvasDraw(Context context) {
		super(context);
		
		para=new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		paint.setAntiAlias(true);
		paint.setColor(Color.BLUE);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeJoin(Paint.Join.ROUND);
		paint.setStrokeWidth(10f);
		
		CirclePaint.setAntiAlias(true);
		CirclePaint.setColor(Color.GREEN);
		CirclePaint.setStyle(Paint.Style.STROKE);
		CirclePaint.setStrokeJoin(Paint.Join.ROUND);
		CirclePaint.setStrokeWidth(10f);
		
     	parent=new LinearLayout(context);
		reset=new Button(context);
		reset.setBackgroundResource(R.drawable.erase);

		parent.addView(reset);
		reset.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Handler handler=new Handler();
				handler.postDelayed(new Runnable(){

					@Override
					public void run() {
						reset.getBackground().setAlpha(500);
					}
				},200);
				reset.getBackground().setAlpha(100);
			path.reset();
			postInvalidate();
			}
		});
	}
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawPath(path, paint);
		canvas.drawPath(CirclePath, CirclePaint);
	}
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float pointX=event.getX();
		float pointY=event.getY();
		switch(event.getAction()){
		case MotionEvent.ACTION_DOWN:
			path.moveTo(pointX, pointY);
			return true;
		case MotionEvent.ACTION_MOVE:
			path.lineTo(pointX, pointY);
			CirclePath.reset();
			CirclePath.addCircle(pointX, pointY, 30, Path.Direction.CW);
			break;
		case MotionEvent.ACTION_UP:
			CirclePath.reset();
			break;
		}
		invalidate();
		return true;
	}

}

