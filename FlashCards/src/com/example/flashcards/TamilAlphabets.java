package com.example.flashcards;

import java.sql.SQLException;
import java.sql.Wrapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.zip.Inflater;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Path.FillType;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class TamilAlphabets extends ActionBarActivity implements 
TextToSpeech.OnInitListener {
	MediaPlayer mp;
	boolean Stoped=false,SpeakBtnClicked=false,SpeakBtnClicked2=false;
	Handler handler;
	LinearLayout AlphebtesLinearLayout;
	boolean killMe=false;
	Map<String,String> map;
	Map<String,String> map2;
	HashMap<String,ArrayList<String>> Multimap;
	TextToSpeech textToSpeech;
	TextView TamilLetters;
	int count=0,id=0,cti=0,cnt1=0,dialogcnt=0,DiaologId2cnt=0;
	ListView list;
	String TagName;
	String[] SelectKey,SelectValue,copykey;
	LinearLayout.LayoutParams Btnpara;
	LinearLayout tamil_alphabetsdialog,parent;
	Dialog Alphbetdialog,Alphbetdialog1,dialog;
	ArrayList<View>copyView;
	Thread TimeCnt;
	View read;
	Button btn3;
	Runnable runnable,runnable2;
	HashMap<String,ArrayList<String>> CopyMultimap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tamil_alphabets);
		textToSpeech=new TextToSpeech(this, this);
		btn3=new Button(TamilAlphabets.this);
		createList(0);
		Btnpara=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
	}
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		Stoped=true;
		SpeakBtnClicked=false;
		//SpeakBtnClicked2=false;
		LinearLayout AlphebtesLinearLayout=(LinearLayout)findViewById(R.id.AlphebtesLinearLayout);
		parent=(LinearLayout)findViewById(R.id.AlphebtesLinearLayout);
		int width=parent.getWidth();
		LinearLayout.LayoutParams para=new LinearLayout.LayoutParams(width/8,
				width/8 /*LinearLayout.LayoutParams.FILL_PARENT)*/);
		
		//list.setLayoutParams(para);
		//createList(0);
	}
	public void createList(int width){
		final HashMap<String,ArrayList<String>> Multimap=new HashMap<String,ArrayList<String>>();
		AlphabetsHashMap getHashmap=new AlphabetsHashMap();
		map= new HashMap<String, String>();
		map2= new HashMap<String, String>();
		
		Multimap.put("அ", new ArrayList<String>(Arrays.asList("அம்மா",String.valueOf(R.drawable.alp_mother),String.valueOf(R.raw.a),String.valueOf(R.raw.mother))));
		Multimap.put("ஆ", new ArrayList<String>(Arrays.asList("ஆமை",String.valueOf(R.drawable.tortoise),String.valueOf(R.raw.aa),String.valueOf(R.raw.tortoise))));
		Multimap.put("இ", new ArrayList<String>(Arrays.asList("இலை",String.valueOf(R.drawable.leaf),String.valueOf(R.raw.e),String.valueOf(R.raw.leaf))));
		Multimap.put("ஈ", new ArrayList<String>(Arrays.asList("ஈ",String.valueOf(R.drawable.fly),String.valueOf(R.raw.ee),String.valueOf(R.raw.fly))));
		Multimap.put("உ", new ArrayList<String>(Arrays.asList("உரல்",String.valueOf(R.drawable.yural),String.valueOf(R.raw.u),String.valueOf(R.raw.yural))));
		Multimap.put("ஊ", new ArrayList<String>(Arrays.asList("ஊஞ்சல்",String.valueOf(R.drawable.unjal),String.valueOf(R.raw.uu),String.valueOf(R.raw.unjal))));
		Multimap.put("எ", new ArrayList<String>(Arrays.asList("எறும்பு",String.valueOf(R.drawable.ant),String.valueOf(R.raw.ya),String.valueOf(R.raw.ant))));
		Multimap.put("ஏ", new ArrayList<String>(Arrays.asList("ஏணி",String.valueOf(R.drawable.ladder),String.valueOf(R.raw.yaa),String.valueOf(R.raw.ladder))));
		Multimap.put("ஐ", new ArrayList<String>(Arrays.asList("ஐந்து",String.valueOf(R.drawable.five),String.valueOf(R.raw.i),String.valueOf(R.raw.five))));
		Multimap.put("ஒ", new ArrayList<String>(Arrays.asList("ஒன்று",String.valueOf(R.drawable.alphabet_one),String.valueOf(R.raw.o),String.valueOf(R.raw.one))));
		Multimap.put("ஓ", new ArrayList<String>(Arrays.asList("ஓணான்",String.valueOf(R.drawable.chameleons),String.valueOf(R.raw.oo),String.valueOf(R.raw.chameleons))));
		Multimap.put("ஔ", new ArrayList<String>(Arrays.asList("ஔவை",String.valueOf(R.drawable.avai),String.valueOf(R.raw.ow),String.valueOf(R.raw.avai))));
		Multimap.put("ஃ", new ArrayList<String>(Arrays.asList("ஃ",String.valueOf(R.drawable.akk),String.valueOf(R.raw.akkk),String.valueOf(R.raw.akkk))));
		
		final ArrayList<HashMap> HashMapCollection=getHashmap.Hashmaps();
		 
		
		 //--------------------------------------------
				final String[] Uirkeys = {"அ","ஆ","இ","ஈ","உ","ஊ","எ","ஏ","ஐ","ஒ","ஓ","ஔ","ஃ"};
				final String[] Uirvalues = {"a","aa","e","ee","ou","ou.","ay","ay.","ai","o","o.","ow","aak"};
		 //----------------------------------------------
				
				final String[] Uirkeys1={"க்","ங்","ச்","ஞ்","ட்","ண்","த்","ந்","ப்","ம்","ய்","ர்","ல்","வ்","ழ்",
						  "ள்","ற்","ன்"};
				final String[] Uirvalues1 = {"ik","ing","each","inj","it","in","ith","ind","ip","im","ye","er","il","ev","ill",
						   "ill.","er.","inn"};
		  //------------------------------------------------------			
				final String[] Uirkeys2={"க","ங","ச","ஞ","ட","ண","த","ந","ப","ம","ய","ர","ல","வ","ழ",
						  "ள","ற","ன"}; 
				final String[] Uirvalues2 = {"ka","na.","sa","ண","ta","naa","tha","nha","pa","ma","ya",
						   "ra","la","va","la..","la.","ra","na"};
		   //-------------------------------------------------------
				final String[] Uirkeys3={"கா","ஙா","சா","ஞா","டா","ணா","தா","நா","பா","மா","யா","ரா","லா","வா","ழா",
						  "ளா","றா","னா"}; 
				final String[] Uirvalues3 = {"கா","ஙா","சா","ஞா","டா","ணா","தா","நா","பா","மா","யா","ரா","லா","வா","ழா",
						  "ளா","றா","னா"};
		   //-------------------------------------------------------	
		   //-------------------------------------------------------
				final String[] Uirkeys4={"கி","ஙி","சி","ஞி","டி","ணி","தி","நி","பி","மி","யி","ரி","லி","வி","ழி",
						  "ளி","றி","னி"}; 
				final String[] Uirvalues4 = {"கி","ஙி","சி","ஞி","டி","ணி","தி","நி","பி","மி","யி","ரி","லி","வி","ழி",
						  "ளி","றி","னி"};
		   //-------------------------------------------------------
		   //-------------------------------------------------------
				final String[] Uirkeys5={"கீ","ஙீ","சீ","ஞீ","டீ","ணீ","தீ","நீ","பீ","மீ","யீ","ரீ","லீ","வீ","ழீ",
						  "ளீ","றீ","னீ"}; 
				final String[] Uirvalues5 = {"கீ","ஙீ","சீ","ஞீ","டீ","ணீ","தீ","நீ","பீ","மீ","யீ","ரீ","லீ","வீ","ழீ",
						  "ளீ","றீ","னீ"};
		   //-------------------------------------------------------	
		   //-------------------------------------------------------
				final String[] Uirkeys6={"கு","ஙு","சு","ஞு","டு","ணு","து","நு","பு","மு","யு","ரு","லு","வு","ழு",
						  "ளு","று","னு"}; 
				final String[] Uirvalues6 = {"கு","ஙு","சு","ஞு","டு","ணு","து","நு","பு","மு","யு","ரு","லு","வு","ழு",
						  "ளு","று","னு"};
		   //-------------------------------------------------------
		   //-------------------------------------------------------
				final String[] Uirkeys7={"கூ","ஙூ","சூ","ஞூ","டூ","ணூ","தூ","நூ","பூ","மூ","யூ","ரூ","லூ","வூ","ழூ",
						  "ளூ","றூ","னூ"}; 
				final String[] Uirvalues7 = {"கூ","ஙூ","சூ","ஞூ","டூ","ணூ","தூ","நூ","பூ","மூ","யூ","ரூ","லூ","வூ","ழூ",
						  "ளூ","றூ","னூ"};
		   //-------------------------------------------------------
		   //-------------------------------------------------------
				final String[] Uirkeys8={"கெ","ஙெ","செ","ஞெ","டெ","ணெ","தெ","நெ","பெ","மெ","யெ","ரெ","லெ","வெ","ழெ",
						  "ளெ","றெ","னெ"}; 
				final String[] Uirvalues8 = {"கெ","ஙெ","செ","ஞெ","டெ","ணெ","தெ","நெ","பெ","மெ","யெ","ரெ","லெ","வெ","ழெ",
						  "ளெ","றெ","னெ"};
		   //-------------------------------------------------------	
		   //-------------------------------------------------------
				final String[] Uirkeys9={"கே","ஙே","சே","ஞே","டே","ணே","தே","நே","பே","மே","யே","ரே","லே","வே","ழே",
						  "ளே","றே","னே"}; 
				final String[] Uirvalues9 = {"கே","ஙே","சே","ஞே","டே","ணே","தே","நே","பே","மே","யே","ரே","லே","வே","ழே",
						  "ளே","றே","னே"};
		   //-------------------------------------------------------	
		   //-------------------------------------------------------
				final String[] Uirkeys10={"கை","ஙை","சை","ஞை","டை","ணை","தை","நை","பை","மை","யை","ரை","லை","வை","ழை",
						  "ளை","றை","னை"}; 
				final String[] Uirvalues10 = {"கை","ஙை","சை","ஞை","டை","ணை","தை","நை","பை","மை","யை","ரை","லை","வை","ழை",
						  "ளை","றை","னை"};
		   //-------------------------------------------------------	
		   //-------------------------------------------------------
				final String[] Uirkeys11={"கொ","ஙொ","சொ","ஞொ","டொ","ணொ","தொ","நொ","பொ","மொ","யொ","ரொ","லொ","வொ","ழொ",
						  "ளொ","றொ","னொ"}; 
				final String[] Uirvalues11 = {"கொ","ஙொ","சொ","ஞொ","டொ","ணொ","தொ","நொ","பொ","மொ","யொ","ரொ","லொ","வொ","ழொ",
						  "ளொ","றொ","னொ"};
		   //-------------------------------------------------------	
		   //-------------------------------------------------------
				final String[] Uirkeys12={"கோ","ஙோ","சோ","ஞோ","டோ","ணோ","தோ","நோ","போ","மோ","யோ","ரோ","லோ","வோ","ழோ",
						  "ளோ","றோ","னோ"}; 
				final String[] Uirvalues12 = {"கோ","ஙோ","சோ","ஞோ","டோ","ணோ","தோ","நோ","போ","மோ","யோ","ரோ","லோ","வோ","ழோ",
						  "ளோ","றோ","னோ"};
		   //-------------------------------------------------------	
		   //-------------------------------------------------------
				final String[] Uirkeys13={"கௌ","ஙௌ","சௌ","ஞௌ","டௌ","ணௌ","தௌ","நௌ","பௌ","மௌ","யௌ","ரௌ","லௌ","வௌ","ழௌ",
						  "ளௌ","றௌ","னௌ"}; 
				final String[] Uirvalues13 = {"கௌ","ஙௌ","சௌ","ஞௌ","டௌ","ணௌ","தௌ","நௌ","பௌ","மௌ","யௌ","ரௌ","லௌ","வௌ","ழௌ",
						  "ளௌ","றௌ","னௌ"};
		   //-------------------------------------------------------	
					
				
				for(int loop=0;loop<14;loop++){	
				if(count==0){
					SelectKey=Uirkeys;
					SelectValue=Uirvalues;
				}else if(count==1){
					SelectKey=Uirkeys1;
					SelectValue=Uirvalues1;
				}else if(count==2){
					SelectKey=Uirkeys2;
					SelectValue=Uirvalues2;
				}else if(count==3){
					SelectKey=Uirkeys3;
					SelectValue=Uirvalues3;
				}else if(count==4){
					SelectKey=Uirkeys4;
					SelectValue=Uirvalues4;
				}else if(count==5){
					SelectKey=Uirkeys5;
					SelectValue=Uirvalues5;
				}else if(count==6){
					SelectKey=Uirkeys6;
					SelectValue=Uirvalues6;
				}else if(count==7){
					SelectKey=Uirkeys7;
					SelectValue=Uirvalues7;
				}else if(count==8){
					SelectKey=Uirkeys8;
					SelectValue=Uirvalues8;
				}else if(count==9){
					SelectKey=Uirkeys9;
					SelectValue=Uirvalues9;
				}else if(count==10){
					SelectKey=Uirkeys10;
					SelectValue=Uirvalues10;
				}else if(count==11){
					SelectKey=Uirkeys11;
					SelectValue=Uirvalues11;
				}else if(count==12){
					SelectKey=Uirkeys12;
					SelectValue=Uirvalues12;
				}else if(count==13){
					SelectKey=Uirkeys13;
					SelectValue=Uirvalues13;
				}
				
				
				for(int i=0;i<SelectKey.length;i++){
					map.put(SelectKey[i],SelectValue[i]);
				}
				for(int j=0;j<SelectKey.length;j++){
					map2.put(SelectValue[j],SelectKey[j]);
				}
		LinearLayout.LayoutParams para=new LinearLayout.LayoutParams(width/8,
				LinearLayout.LayoutParams.FILL_PARENT);
		Activity activity = TamilAlphabets.this;
		TamilLetters=new TextView(TamilAlphabets.this);
		AlphebtesLinearLayout=(LinearLayout)findViewById(R.id.AlphebtesLinearLayout);
		list=new ListView(this);
		GradientDrawable gd=new GradientDrawable();
		//GradientDrawable bgShape = (GradientDrawable)list.getBackground();
		//bgShape.setColor(Color.BLUE);
		gd.setStroke(4, Color.WHITE);
		list.setBackgroundColor(Color.parseColor("#A9D0F5"));
		//list.setBackgroundDrawable(gd);
		ListAdapterTamil LstAdapter=new ListAdapterTamil(TamilAlphabets.this,map,map2,SelectKey,SelectValue,activity,width);
		View header = (View)getLayoutInflater().inflate(R.layout.header,null); 
		read=header.findViewById(R.id.Read);
		read.setTag(id++);
		read.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {	
				final View cpview=v;
				//final int btnid=Integer.parseInt(v.getTag().toString());
				Handler Readhandler=new Handler();
				Readhandler.postDelayed(new Runnable(){

					@Override
					public void run() {		
						cpview.getBackground().setAlpha(500);
					}	
				},300);
				cpview.getBackground().setAlpha(100);
				String id= v.getTag().toString();
				if(id.equalsIgnoreCase("0")){
					GradientDrawable gd=new GradientDrawable();
					gd.setCornerRadius(5);
					gd.setStroke(10, Color.parseColor("#00000000"));
					LinearLayout.LayoutParams para=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					    Alphbetdialog=new Dialog(TamilAlphabets.this);
					    Alphbetdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					    Alphbetdialog.setContentView(R.layout.tamil_alphabetsdialog);
					    LinearLayout AlphabetLayout=(LinearLayout)Alphbetdialog.findViewById(R.id.alphabetlayout1);
					    LinearLayout AlphabetLayout2=(LinearLayout)Alphbetdialog.findViewById(R.id.alphabetlayout2);
					    LinearLayout AlphabetLayout3=(LinearLayout)Alphbetdialog.findViewById(R.id.alphabetlayout3);
					    LinearLayout AlphabetLayout4=(LinearLayout)Alphbetdialog.findViewById(R.id.alphabetlayout4);
				    
				    int dialogbtn1id=0;
				    for(int j=0;j<4;j++){
				    for(int i=0;i<4;i++){
				    final Button btn1=new Button(TamilAlphabets.this);
				    btn1.setBackgroundResource(R.drawable.tamilalphabet_btn);
				    para.rightMargin=5;
				    para.bottomMargin=3;
				    btn1.setLayoutParams(para);
				    btn1.setPadding(10, 10, 10, 10);
				    btn1.setId(dialogbtn1id);
				    btn1.setOnClickListener(new OnClickListener() {		
						@Override
						public void onClick(View v) {
							Stoped=true;
							dialog=new Dialog(TamilAlphabets.this);
							dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
							dialog.setContentView(R.layout.tamil_alphabet_speak_word);
							LinearLayout Letter=(LinearLayout)dialog.findViewById(R.id.Letter);
							LinearLayout word=(LinearLayout)dialog.findViewById(R.id.Word);
							LinearLayout.LayoutParams Letterpara=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
									LinearLayout.LayoutParams.WRAP_CONTENT);
							Letterpara.setMargins(20, 0, 0, 10);
							TagName=v.getTag().toString();
							GradientDrawable gd=new GradientDrawable();
							gd.setStroke(5,Color.WHITE);
							gd.setCornerRadius(10);
							TextView Txtletter=new TextView(TamilAlphabets.this);
							Button WordImage=new Button(TamilAlphabets.this);
							TextView Txtword=new TextView(TamilAlphabets.this);
							Txtletter.setText(TagName);
							Txtletter.setTextColor(Color.WHITE);
							Txtletter.setTextSize(TypedValue.COMPLEX_UNIT_SP,25);
							WordImage.setBackgroundResource(Integer.parseInt(Multimap.get(TagName).get(1)));
							
							Handler handler=new Handler();
							handler.postDelayed(new Runnable(){

								@Override
								public void run() {
									
									ModeSound(Integer.parseInt(Multimap.get(TagName).get(3)));
								}
								
							},700);
							ModeSound(Integer.parseInt(Multimap.get(TagName).get(2)));	 
							Txtword.setText(Multimap.get(TagName).get(0));
							Txtword.setTextColor(Color.WHITE);
							Txtword.setTextSize(TypedValue.COMPLEX_UNIT_SP,25);
							Letter.addView(Txtletter);			
							Letter.setLayoutParams(Letterpara);
							Letter.addView(WordImage);
							word.addView(Txtword);
							word.setLayoutParams(Letterpara);
							word.setGravity(Gravity.CENTER);			
							dialog.getWindow().setBackgroundDrawable(gd);
							dialog.show();
							if(dialog.isShowing()){
								SpeakBtnClicked=false;
							}
						}
					});
				    
				    if(j==0){
				    btn1.setTypeface(null, Typeface.BOLD);
				    btn1.setText(Uirkeys[dialogbtn1id]);	
				    AlphabetLayout.addView(btn1);
				    btn1.setTag(Uirkeys[dialogbtn1id]);
				    }
				    else if(j==1){
				    btn1.setTypeface(null, Typeface.BOLD);
				    AlphabetLayout2.addView(btn1);
				    btn1.setText(Uirkeys[dialogbtn1id]);
				    btn1.setTag(Uirkeys[dialogbtn1id]);
				    }
				    else if(j==2){
				    btn1.setTypeface(null, Typeface.BOLD);
				    AlphabetLayout3.addView(btn1);
				    btn1.setText(Uirkeys[dialogbtn1id]);
				    btn1.setTag(Uirkeys[dialogbtn1id]);
				    }
				    else if(j==3){
				        btn1.setTypeface(null, Typeface.BOLD);
					    AlphabetLayout4.addView(btn1);
					    if(i!=1 && i!=2){
					    btn1.setText(Uirkeys[dialogbtn1id]);
					    btn1.setTag(Uirkeys[dialogbtn1id]);
					    }
				    if(i==1){
				    	 btn1.setBackgroundResource(R.drawable.tamil_letter_read);
				    	 btn1.setOnClickListener(new OnClickListener() {	
							@Override
							public void onClick(View v) {
								LinearLayout l1=(LinearLayout)Alphbetdialog.findViewById(R.id.alphabetlayout1);							
								LinearLayout l2=(LinearLayout)Alphbetdialog.findViewById(R.id.alphabetlayout2);							
								LinearLayout l3=(LinearLayout)Alphbetdialog.findViewById(R.id.alphabetlayout3);							
								LinearLayout l4=(LinearLayout)Alphbetdialog.findViewById(R.id.alphabetlayout4);	
								copyView=new ArrayList<View>();
								ViewGroup vg=(ViewGroup)l1;
								vg.getChildCount();
								for(int i=0;i<4;i++){
									for(int j=0;j<4;j++){
										if(i==0)
										copyView.add(l1.getChildAt(j));
										else if(i==1)
											copyView.add(l2.getChildAt(j));
										else if(i==2)
											copyView.add(l3.getChildAt(j));
										else if(i==3){
											copyView.add(l4.getChildAt(j));
											if(j==0){
												break;
											}
										}
										
									}
								}
								
									//try {	
							final	Handler handler=new Handler();	
								Runnable runnable = new Runnable() {
							        @Override
							        public void run() {
							            for (int i = 0; i<Uirkeys.length; i++) {
							            	if(Stoped==false){
							                final int value = i;
							                try {     	
							                    Thread.sleep(700);
							                } catch (InterruptedException e) {
							                    e.printStackTrace();
							                }
							                handler.post(new Runnable() {
							                    @Override
							                    public void run() {
							                    	copyView.get(cnt1).getBackground().setAlpha(100);
													ModeSound(Integer.parseInt(Multimap.get(Uirkeys[cnt1]).get(2)));						
							                    }
							                });
                                            try {	
							                    Thread.sleep(800);
							                } catch (InterruptedException e) {
							                    e.printStackTrace();
							                }
                                            handler.post(new Runnable() {
							                    @Override
							                    public void run() {
							                    	copyView.get(cnt1).getBackground().setAlpha(500);
							                    	cnt1++;
							                    	if(cnt1==13)
							                    	SpeakBtnClicked=false;
							                    }
							                });
							            }
							        }
							      }
							    };
							    if(SpeakBtnClicked==false){
							    new Thread(runnable).start();	
							    cnt1=0;  
							    SpeakBtnClicked=true;
							    Stoped=false;
							    }
							}
						});
				    	 
				         }
				    
				    if(i==2){
				    	 btn1.setBackgroundResource(R.drawable.write);
				    	 btn1.setOnClickListener(new OnClickListener() {		
							@Override
							public void onClick(View v) {
								Stoped=true;
								SpeakBtnClicked=false;
								SpeakBtnClicked2=false;
								Dialog CanvasDialog=new Dialog(TamilAlphabets.this);
								CanvasDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
								CanvasDraw dw=new CanvasDraw(TamilAlphabets.this);
								dw.getDrawingCache();
								CanvasDialog.setContentView(dw);
								CanvasDialog.addContentView(dw.parent,dw.para);
								CanvasDialog.show();
								
							}
						});
				    	break;
				          }
				         
				        }
				    dialogbtn1id++;
				      }
				    }
				    Alphbetdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
				 //   dialog.get
				    Alphbetdialog.show();
/*H*/               }else{
	                int Listid=Integer.parseInt(v.getTag().toString());
	                if(Listid==1){
	                	 copykey=Uirkeys1;
	                }else if(Listid==2){
	                	 copykey=Uirkeys2;
	                }else if(Listid==3){
	                	 copykey=Uirkeys3;
	                }else if(Listid==4){
	                	 copykey=Uirkeys4;
	                }else if(Listid==5){
	                	 copykey=Uirkeys5;
	                }else if(Listid==6){
	                	 copykey=Uirkeys6;
	                }else if(Listid==7){
	                	 copykey=Uirkeys7;
	                }else if(Listid==8){
	                	 copykey=Uirkeys8;
	                }else if(Listid==9){
	                	 copykey=Uirkeys9;
	                }else if(Listid==10){
	                	 copykey=Uirkeys10;
	                }else if(Listid==11){
	                	 copykey=Uirkeys11;
	                }else if(Listid==12){
	                	 copykey=Uirkeys12;
	                }else if(Listid==13){
	                	 copykey=Uirkeys13;
	                }
					GradientDrawable gd=new GradientDrawable();
					gd.setCornerRadius(5);
					gd.setStroke(10, Color.parseColor("#00000000"));
					dialog=new Dialog(TamilAlphabets.this);
			        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			        dialog.setContentView(R.layout.tamil_alphabetsdialog);
				    LinearLayout AlphabetLayout=(LinearLayout)dialog.findViewById(R.id.alphabetlayout1);
				    LinearLayout AlphabetLayout2=(LinearLayout)dialog.findViewById(R.id.alphabetlayout2);
				    LinearLayout AlphabetLayout3=(LinearLayout)dialog.findViewById(R.id.alphabetlayout3);
				    LinearLayout AlphabetLayout4=(LinearLayout)dialog.findViewById(R.id.alphabetlayout4);
				    LinearLayout AlphabetLayout5=(LinearLayout)dialog.findViewById(R.id.alphabetlayout5);
				    int dialogbtn1id=0;
				    DiaologId2cnt=0;
				    CopyMultimap=HashMapCollection.get(Listid); 
				    for(int j=0;j<5;j++){
				    for(int i=0;i<4;i++){
				    Button btn2=new Button(TamilAlphabets.this);
				    btn2.setId(dialogbtn1id++);
				    btn2.setBackgroundResource(R.drawable.tamilalphabet_btn);
				    btn2.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {

							LinearLayout.LayoutParams wordimagepara=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
									LinearLayout.LayoutParams.WRAP_CONTENT);
							Stoped=true;
							Dialog Imagedialog=new Dialog(TamilAlphabets.this);
							Imagedialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
							Imagedialog.setContentView(R.layout.tamil_alphabet_speak_word);
							LinearLayout Letter=(LinearLayout)Imagedialog.findViewById(R.id.Letter);
							LinearLayout word=(LinearLayout)Imagedialog.findViewById(R.id.Word);
							LinearLayout.LayoutParams Letterpara=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
									LinearLayout.LayoutParams.WRAP_CONTENT);
							Letterpara.setMargins(20, 0, 0, 10);
							TagName=v.getTag().toString();
							GradientDrawable gd=new GradientDrawable();
							gd.setStroke(5,Color.WHITE);
							gd.setCornerRadius(10);
							TextView Txtletter=new TextView(TamilAlphabets.this);
							Button WordImage=new Button(TamilAlphabets.this);
							TextView Txtword=new TextView(TamilAlphabets.this);
							Txtletter.setText(TagName);
							Txtletter.setTextColor(Color.WHITE);
							Txtletter.setTextSize(TypedValue.COMPLEX_UNIT_SP,25);
							WordImage.setBackgroundResource(Integer.parseInt(CopyMultimap.get(TagName).get(2)));
							wordimagepara.setMargins(5, 5, 5, 5);
							WordImage.setLayoutParams(wordimagepara);
							Handler handler=new Handler();
							handler.postDelayed(new Runnable(){
								@Override
								public void run() {
									ModeSound(Integer.parseInt(CopyMultimap.get(TagName).get(4)));
								}
							},700);
							ModeSound(Integer.parseInt(CopyMultimap.get(TagName).get(3)));	 
							Txtword.setText(CopyMultimap.get(TagName).get(1));
							Txtword.setTextColor(Color.WHITE);
							Txtword.setTextSize(TypedValue.COMPLEX_UNIT_SP,25);
							Letter.addView(Txtletter);			
							Letter.setLayoutParams(Letterpara);
							Letter.addView(WordImage);
							word.addView(Txtword);
							word.setLayoutParams(Letterpara);
							word.setGravity(Gravity.CENTER);			
							Imagedialog.getWindow().setBackgroundDrawable(gd);
							Imagedialog.show();
							if(Imagedialog.isShowing()){
								SpeakBtnClicked=false;
							}
						}
					});
				    if(j==0){	
				    Btnpara.width=parent.getWidth()/10;
				    btn2.setTypeface(null, Typeface.BOLD);
				    btn2.setLayoutParams(Btnpara);				    
				    AlphabetLayout.addView(btn2);
				    btn2.setTag(CopyMultimap.get(copykey[DiaologId2cnt]).get(0));
				    btn2.setText(CopyMultimap.get(copykey[DiaologId2cnt]).get(0));
				    }else if(j==1){
				    Btnpara.leftMargin=0;
				    btn2.setTypeface(null, Typeface.BOLD);
				    Btnpara.width=parent.getWidth()/12;
					btn2.setLayoutParams(Btnpara);
				    AlphabetLayout2.addView(btn2);
				    btn2.setTag(CopyMultimap.get(copykey[DiaologId2cnt]).get(0));
				    btn2.setText(CopyMultimap.get(copykey[DiaologId2cnt]).get(0));
				    }else if(j==2){
				   	Btnpara.leftMargin=0;
				   	Btnpara.width=parent.getWidth()/12;
				   	btn2.setTypeface(null, Typeface.BOLD);
				    btn2.setLayoutParams(Btnpara);
			    	AlphabetLayout3.addView(btn2);
				    btn2.setTag(CopyMultimap.get(copykey[DiaologId2cnt]).get(0));
				    btn2.setText(CopyMultimap.get(copykey[DiaologId2cnt]).get(0));
				    }else if(j==3){
				    Btnpara.leftMargin=0;
				    Btnpara.width=parent.getWidth()/12;
				    btn2.setTypeface(null, Typeface.BOLD);
				    btn2.setLayoutParams(Btnpara);
				    AlphabetLayout4.addView(btn2);
				    btn2.setTag(CopyMultimap.get(copykey[DiaologId2cnt]).get(0));
				    btn2.setText(CopyMultimap.get(copykey[DiaologId2cnt]).get(0));
				    }else if(j==4){ 
				    Btnpara.width=parent.getWidth()/12;
				    Btnpara.leftMargin=0;
				    btn2.setTypeface(null, Typeface.BOLD);
				    btn2.setLayoutParams(Btnpara);
			    	AlphabetLayout5.addView(btn2);
				    	if(i!=2 && i!=3){
				    		btn2.setTypeface(null, Typeface.BOLD);
				    		btn2.setTag(CopyMultimap.get(copykey[DiaologId2cnt]).get(0));   	
				    		btn2.setText(CopyMultimap.get(copykey[DiaologId2cnt]).get(0));
				    	}if(i==2){
				    		btn2.setBackgroundResource(R.drawable.tamil_letter_read);
				    		btn2.setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									cnt1=0;
									copyView=new ArrayList<View>();
									copyView.clear();						
									LinearLayout l1=(LinearLayout)dialog.findViewById(R.id.alphabetlayout1);							
									LinearLayout l2=(LinearLayout)dialog.findViewById(R.id.alphabetlayout2);							
									LinearLayout l3=(LinearLayout)dialog.findViewById(R.id.alphabetlayout3);							
									LinearLayout l4=(LinearLayout)dialog.findViewById(R.id.alphabetlayout4);
									LinearLayout l5=(LinearLayout)dialog.findViewById(R.id.alphabetlayout5);
										for(int i=0;i<5;i++){
										for(int j=0;j<4;j++){
											if(i==0)
											copyView.add(l1.getChildAt(j));
											else if(i==1)
												copyView.add(l2.getChildAt(j));
											else if(i==2)
												copyView.add(l3.getChildAt(j));
											else if(i==3)
												copyView.add(l4.getChildAt(j));
											else if(i==4){
												copyView.add(l5.getChildAt(j));
												if(j==2){
													break;
												}
											}
										}
									}
									ViewGroup vg=(ViewGroup)l1;
									vg.getChildCount();
									handler=new Handler();	
									runnable2 = new Runnable() {
								        @Override
								        public void run() {
								        	
								            for (int i = 0; i<Uirkeys1.length; i++) {
								            	if(Stoped==false){
								                final int value = i;
								                try {
								                	
								                    Thread.sleep(700);
								                } catch (InterruptedException e) {
								                    e.printStackTrace();
								                }
								                handler.post(new Runnable() {
								                    @Override
								                    public void run() {
								               	
								                    	copyView.get(cnt1).getBackground().setAlpha(100);
														ModeSound(Integer.parseInt(CopyMultimap.get(copykey[cnt1]).get(3)));						
								                    }
								                });
	                                            try {	
								                    Thread.sleep(800);
								                } catch (InterruptedException e) {
								                    e.printStackTrace();
								                }
	                                            handler.post(new Runnable() {
								                    @Override
								                    public void run() {
								                    	
								                    	copyView.get(cnt1).getBackground().setAlpha(500);
								                    	cnt1++;
								                    	if(cnt1==18){
								                    		SpeakBtnClicked=false;	
								                    		cnt1=0;
								                    	}
								                    }
								                });
								            }
								          }
								        }
								    };
								    if(SpeakBtnClicked==false){
								    new Thread(runnable2).start();
								    cnt1=0;
								    SpeakBtnClicked=true;
								    Stoped=false;
								    }/*else{
								    	Stoped=true;
								    }*/
								}
							});
				    	}
				    	   if(i==3){
				    		     btn2.setBackgroundResource(R.drawable.write);
						    	 btn2.setOnClickListener(new OnClickListener() {		
									@Override
									public void onClick(View v) {
										Stoped=true;
										SpeakBtnClicked=false;
										SpeakBtnClicked2=false;
										Dialog CanvasDialog=new Dialog(TamilAlphabets.this);
										CanvasDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
										CanvasDraw dw=new CanvasDraw(TamilAlphabets.this);
										dw.getDrawingCache();
										CanvasDialog.setContentView(dw);
										CanvasDialog.addContentView(dw.parent,dw.para);
										CanvasDialog.show();
										
									}
								});
						    	break;
						          }
				    	 
				    
				    }
				    DiaologId2cnt++;
				     }
				    }
				    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
				    dialog.show();
				}
			}
		});
		LinearLayout.LayoutParams Listpara=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		read.setBackgroundResource(R.drawable.read_btn);
		list.addHeaderView(header);
		Listpara.leftMargin=5;
		list.setLayoutParams(Listpara);
		list.setAdapter(LstAdapter);
		AlphebtesLinearLayout.addView(list);
		TamilLetters.setText("aa");
		TamilLetters.setLayoutParams(para);
		count++;
		}	

	}
	@Override
	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
			int result = textToSpeech.setLanguage(Locale.US);
			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e("error", "This Language is not supported");
			}
		} else {
			Log.e("error", "Initilization Failed!");
		}
		
	}
	public void TeaxtToSpeach(String Name){
		AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		int amStreamMusicMaxVol = am.getStreamMaxVolume(am.STREAM_MUSIC);
		am.setStreamVolume(am.STREAM_MUSIC, amStreamMusicMaxVol, 0);
		textToSpeech.setSpeechRate(0.7f);
		textToSpeech.speak(Name, TextToSpeech.QUEUE_FLUSH, null);
	}
	public void ModeSound(int VoiceID){
	
		mp = MediaPlayer.create(this, VoiceID);
		mp.start();
			mp.setOnCompletionListener(new OnCompletionListener() {
				
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
				}
			});
	}
}
class ListAdapterTamil extends BaseAdapter{
	Context context;
Map<String,String> map=new HashMap<String, String>();
Map<String,String> map2=new HashMap<String, String>();
private Activity activity;
String[] key;
String[] values;
int width;
	public  ListAdapterTamil(Context context,Map<String,String> map,
			Map<String,String> map2,String[] key,String[] values,Activity activity,int width){
		this.context=context;
		this.map=map;
		this.map2=map2;
		this.key=key;
		this.values=values;
		this.activity=activity;
		this.width=width;
	}
	@Override
	
	public int getCount() {
		return key.length;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
			return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.tamil_alphabets, parent, false);
		TextView txt=new TextView(activity);
		Button Read=new Button(activity);
		LinearLayout AlphebtesLinearLayout=(LinearLayout)rowView.findViewById(R.id.AlphebtesLinearLayout);
		AlphebtesLinearLayout.addView(txt);
		    txt.setTextSize(25);
		    txt.setTextColor(Color.WHITE);
			txt.setText(map2.get(values[position]));
			txt.setPadding( 10,10,50,50);	
			return rowView;
	}
}
