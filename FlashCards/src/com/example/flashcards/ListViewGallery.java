package com.example.flashcards;

//import com.example.flashcardsdemo.R;

//import com.example.myfirsstpro.HomeLogo;
//import com.example.myfirsstpro.HomeScreen;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;

public class ListViewGallery extends ActionBarActivity {
	Context context;
	 ListView Listview;
     String[] Titles;
     String[] Description;
     LinearLayout Parent;
     //String[] Hint;
     int[] images={R.drawable.alphabets_default_image,R.drawable.animals_listimage,
    		 R.drawable.family_listimage,R.drawable.numbers_default_image
     };
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listview);
       Resources res=getResources();
            
           Parent=(LinearLayout)findViewById(R.id.linear1);
    	   Titles=res.getStringArray(R.array.Title);
           Description=res.getStringArray(R.array.Description);
           //Hint=res.getStringArray(R.array.Hint);
           Listview=(ListView)findViewById(R.id.listView);
           Listadapter lstAdapter=new Listadapter(ListViewGallery.this,Titles,Description,images);
           Listview.setAdapter(lstAdapter);
           Listview.setBackgroundColor(Color.parseColor("#00000000"));
           Parent.setBackgroundResource(R.drawable.tamiltheme1);
       Listview.setOnItemClickListener(new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			try{
				final Dialog dialog = new Dialog(ListViewGallery.this);
				//View row=LayoutInflater.from(this).inflate(R.layout.dialog_layout, parent,false);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.dialog_layout);
				Window window = dialog.getWindow();
				window.setLayout(Parent.getHeight()/2, Parent.getHeight()/2);
				dialog.show();
				final int pos=position;
				ImageView ImgView=(ImageView)dialog.findViewById(R.id.learn);
				ImageView Review=(ImageView)dialog.findViewById(R.id.revise);
				ImgView.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if(pos==0){
							//Alphabets
							Intent intent=new Intent(ListViewGallery.this,TamilAlphabets.class);
							startActivity(intent);
							dialog.cancel();
							
						}
						if(pos==1){
							//Animals
							Intent intent=new Intent(ListViewGallery.this,LearnLayoutAnimals.class);
							intent.putExtra("ListValue", 2);
							startActivity(intent);
							dialog.cancel();
						}
						if(pos==2){
							//Family
							Intent intent=new Intent(ListViewGallery.this,LearnLayoutAnimals.class);
							intent.putExtra("ListValue", 3);
							startActivity(intent);
							dialog.cancel();
						}
						if(pos==3){
							//Numbers
							Intent intent=new Intent(ListViewGallery.this,LearnLayoutAnimals.class);
							intent.putExtra("ListValue", 0);
							startActivity(intent);
							dialog.cancel();
						}
							
					}
				});
		//-------------------------------Review---------------------------------
				Review.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Intent intent=new Intent(ListViewGallery.this,ReviewLayout.class);
						intent.putExtra("ListValue", 2);
						startActivity(intent);
						dialog.cancel();
						
					}
				});
			}catch(Exception e){
				Log.v("dd",e.toString());
			}
			
			
		}
	});
	}
}
class Listadapter extends BaseAdapter{

Context context;
String[] TitleArray;
String[]DescriptionArray;
int[] imgs;
//String[] Hint;
	public Listadapter(Context c, String[] Titles,String[] Description,int[] imgs) {
		//super(c, R.layout.activity_main);
		this.context=c;
		this.TitleArray=Titles;
		this.DescriptionArray=Description;
		this.imgs=imgs;
		//this.Hint=Hint;
		// TODO Auto-generated constructor stub
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return TitleArray.length;
	}
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflator=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
		View row=inflator.inflate(R.layout.activity_main, parent,false);
		ImageView myimage=(ImageView)row.findViewById(R.id.imageView);
	    TextView Title=(TextView)row.findViewById(R.id.textView1);
		TextView Description=(TextView)row.findViewById(R.id.textView2);
		RelativeLayout parent1=(RelativeLayout)row.findViewById(R.id.parent);
		parent1.setBackgroundColor(Color.parseColor("#00000000"));
		myimage.setImageResource(imgs[position]);
		myimage.setScaleType(ImageView.ScaleType.FIT_XY);
		Title.setText(TitleArray[position]);
		Description.setText(DescriptionArray[position]);
		return row;
		
	}	
}