package com.example.flashcards;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import android.R.integer;

public class TamilHashMap {
   public ArrayList<HashMap> HashMap(){
	   
	 //-----------------------Animals----------------------------------------------------
	   
	 		final HashMap<String,Object[]> Multimap1=new HashMap<String,Object[]>();	
	 		Multimap1.put("0",new Object[]{R.drawable.anteateronclick,R.drawable.bearonclick,
	 				R.drawable.camelonclick,R.drawable.catonclick,R.drawable.cougaronclick,
	 				R.drawable.cowonclick,R.drawable.dogonclick,
	 				R.drawable.elephantonclick,R.drawable.frogonclick,R.drawable.gorillaonclick,
	 				R.drawable.hippopotamusonclick,R.drawable.horseonclick,R.drawable.jaguaronclick,
	 				R.drawable.liononclick,R.drawable.pigonclick,R.drawable.sheeponclick,R.drawable.tigeronclick,
	 				R.drawable.wolfonclick});
	 		Multimap1.put("1",new Object[]{R.drawable.anteater_org,R.drawable.bear_org,R.drawable.camel_org,
	 				R.drawable.cat_org,R.drawable.cougar_org,R.drawable.cow_org,R.drawable.dog_org,
	 				R.drawable.elephant_org,R.drawable.frog_org,R.drawable.gorilla_org,R.drawable.hippopotamus_org,
	 				R.drawable.horse_org,R.drawable.jaguar_org,R.drawable.lion_org,R.drawable.pig_org,R.drawable.sheep_org,
	 				R.drawable.tiger_org,R.drawable.wolf_org});
	 		Multimap1.put("2",new Object[]{R.drawable.anteater,R.drawable.bear,R.drawable.camel,
	 				R.drawable.cat,R.drawable.cougar,R.drawable.cow,R.drawable.dog,R.drawable.elephant,
	 				R.drawable.frog,R.drawable.gorilla,R.drawable.hippopotamus,R.drawable.horse,
	 				R.drawable.jaguar,R.drawable.lion,R.drawable.pig,R.drawable.sheep,R.drawable.tiger,
	 				R.drawable.wolf});
	 		Multimap1.put("3",new Object[]{R.raw.anteater,R.raw.bear,R.raw.camel,R.raw.cat,R.raw.cougar,R.raw.cow,
	 				R.raw.dog,R.raw.elephant,R.raw.frog,R.raw.gorilla,R.raw.hippopotamus,R.raw.horse,
	 				R.raw.jaguar,R.raw.lion,R.raw.pig,R.raw.sheep,R.raw.tiger,R.raw.wolf});
	 		Multimap1.put("4",new Object[]{R.drawable.a,R.drawable.b,R.drawable.c,R.drawable.d,R.drawable.e,R.drawable.f
	 				,R.drawable.g,R.drawable.h,R.drawable.i,R.drawable.j,R.drawable.k,R.drawable.l
	 				,R.drawable.m,R.drawable.n,R.drawable.o,R.drawable.p,R.drawable.q,R.drawable.r
	 				,R.drawable.s,R.drawable.t,R.drawable.u,R.drawable.v,R.drawable.w,R.drawable.x
	 				,R.drawable.y,R.drawable.z});
	 		Multimap1.put("5",new Object[]{"எறும்புண்ணி","கரடி","ஒட்டகம்","பூனை","கோகர்","மாடு","நாய்","யானை",
	 				"தவளை","மனிதக்குரங்கு","நீர்யானை","குதிரை","சிறுத்தை ","சிங்கம்","பன்றி","ஆடு","புலி","ஓநாய்"
	 				});
	 		Multimap1.put("6",new Object[]{R.raw.anteater_tamil,R.raw.bear_tamil,R.raw.camel_tamil,R.raw.cat_tamil,
	 				R.raw.cougar_tamil,R.raw.cow_tamil,R.raw.dog_tamil,R.raw.elephant_tamil,R.raw.frog_tamil,R.raw.gorilla_tamil,
	 				R.raw.hippopotamus_tamil,R.raw.horse_tamil,R.raw.jaguar_tamil,R.raw.lion_tamil,R.raw.pig_tamil,R.raw.goat_tamil,
	 				R.raw.tiger_tamil,R.raw.wolf_tamil});
	 		
	 //-----------------------Family-----------------------------------------------------
	 		
	 		final HashMap<String,Object[]> Multimap2=new HashMap<String,Object[]>();	
			Multimap2.put("0",new Object[]{R.drawable.grandfather,R.drawable.grandmother,R.drawable.uncle,
					R.drawable.father,R.drawable.mother,R.drawable.aunt,R.drawable.brother,R.drawable.sister,
					R.drawable.you,R.drawable.nephew,R.drawable.niece,R.drawable.son,
					R.drawable.daughter,R.drawable.grandson,R.drawable.granddaughter});
			Multimap2.put("1",new Object[]{R.drawable.family_relationship_tamil,R.drawable.family_relationship_tamil,R.drawable.family_relationship_tamil,
					R.drawable.family_relationship_tamil,R.drawable.family_relationship_tamil,R.drawable.family_relationship_tamil,R.drawable.family_relationship_tamil,
					R.drawable.family_relationship_tamil,R.drawable.family_relationship_tamil,R.drawable.family_relationship_tamil,R.drawable.family_relationship_tamil,
					R.drawable.family_relationship_tamil,R.drawable.family_relationship_tamil,R.drawable.family_relationship_tamil,R.drawable.family_relationship_tamil});
			Multimap2.put("2",new Object[]{R.drawable.grandfather,R.drawable.grandmother,R.drawable.uncle,
					R.drawable.father,R.drawable.mother,R.drawable.aunt,R.drawable.brother,R.drawable.sister,
					R.drawable.you,R.drawable.nephew,R.drawable.niece,R.drawable.son,
					R.drawable.daughter,R.drawable.grandson,R.drawable.granddaughter});
			Multimap2.put("3",new Object[]{R.raw.family_grandfather,R.raw.family_grandmother,R.raw.family_uncle,
					R.raw.family_father,R.raw.family_mother,R.raw.family_aunt,R.raw.family_brother,R.raw.family_sister,
					R.raw.family_you,R.raw.family_nephew,R.raw.family_niece,R.raw.family_son,R.raw.family_daughter,
					R.raw.family_grandson,R.raw.family_granddaughter});
			Multimap2.put("4",new Object[]{R.drawable.a,R.drawable.b,R.drawable.c,R.drawable.d,R.drawable.e,R.drawable.f
					,R.drawable.g,R.drawable.h,R.drawable.i,R.drawable.j,R.drawable.k,R.drawable.l
					,R.drawable.m,R.drawable.n,R.drawable.o,R.drawable.p,R.drawable.q,R.drawable.r
					,R.drawable.s,R.drawable.t,R.drawable.u,R.drawable.v,R.drawable.w,R.drawable.x
					,R.drawable.y,R.drawable.z});
			Multimap2.put("5",new Object[]{"தாத்தா","பாட்டி","மாமா","தந்தை","தாய்","அத்தை-சித்தி","சகோதரன்","சகோதரி",
					"நீ","மருமகன்","மருமகள்","மகன்","மகள் ","பேரன்","பேத்தி"
					});
			Multimap2.put("6",new Object[]{R.raw.family_grandfather,R.raw.family_grandmother,R.raw.family_uncle,
					R.raw.family_father,R.raw.family_mother,R.raw.family_aunt,R.raw.family_brother,R.raw.family_sister,
					R.raw.family_you,R.raw.family_nephew,R.raw.family_niece,R.raw.family_son,R.raw.family_daughter,
					R.raw.family_grandson,R.raw.family_granddaughter});
	 //-----------------------Numbers----------------------------------------------------
	   
		final HashMap<String,Object[]> Multimap0=new HashMap<String,Object[]>();	
		Multimap0.put("0",new Object[]{R.drawable.one,R.drawable.two,R.drawable.three,
				R.drawable.four,R.drawable.five,R.drawable.six,R.drawable.seven,R.drawable.eight,
				R.drawable.nine,R.drawable.ten,R.drawable.eleven,R.drawable.twelve,
				R.drawable.thirteen,R.drawable.fourteen,R.drawable.fifteen,R.drawable.sixteen,R.drawable.seveenteen,
				R.drawable.eighteen,R.drawable.nineteen,R.drawable.twenty});
		Multimap0.put("1",new Object[]{R.drawable.one,R.drawable.two,R.drawable.three,
				R.drawable.four,R.drawable.five,R.drawable.six,R.drawable.seven,R.drawable.eight,
				R.drawable.nine,R.drawable.ten,R.drawable.eleven,R.drawable.twelve,
				R.drawable.thirteen,R.drawable.fourteen,R.drawable.fifteen,R.drawable.sixteen,R.drawable.seveenteen,
				R.drawable.eighteen,R.drawable.nineteen,R.drawable.twenty});
		Multimap0.put("2",new Object[]{R.drawable.one,R.drawable.two,R.drawable.three,
				R.drawable.four,R.drawable.five,R.drawable.six,R.drawable.seven,R.drawable.eight,
				R.drawable.nine,R.drawable.ten,R.drawable.eleven,R.drawable.twelve,
				R.drawable.thirteen,R.drawable.fourteen,R.drawable.fifteen,R.drawable.sixteen,R.drawable.seveenteen,
				R.drawable.eighteen,R.drawable.nineteen,R.drawable.twenty});
		Multimap0.put("3",new Object[]{R.raw.one_numbers,R.raw.two_numbers,R.raw.three_numbers,R.raw.four_numbers,R.raw.five_numbers,
				R.raw.six_numbers,R.raw.seven_numbers,R.raw.eight_numbers,R.raw.nine_numbers,R.raw.ten_numbers,
				R.raw.eleven_numbers,R.raw.twelve_numbers,R.raw.thirteen_numbers,R.raw.fourteen_numbers,
				R.raw.fifteen_numbers,R.raw.sixteen_numbers,R.raw.seventeen_numbers,R.raw.eighteen_numbers,
				R.raw.nineteen_numbers,R.raw.twenty_numbers});
		Multimap0.put("4",new Object[]{R.drawable.a,R.drawable.b,R.drawable.c,R.drawable.d,R.drawable.e,R.drawable.f
				,R.drawable.g,R.drawable.h,R.drawable.i,R.drawable.j,R.drawable.k,R.drawable.l
				,R.drawable.m,R.drawable.n,R.drawable.o,R.drawable.p,R.drawable.q,R.drawable.r
				,R.drawable.s,R.drawable.t,R.drawable.u,R.drawable.v,R.drawable.w,R.drawable.x
				,R.drawable.y,R.drawable.z});
		Multimap0.put("5",new Object[]{"ஒன்று","இரண்டு","மூன்று","நான்கு","ஐந்து","ஆறு","ஏழு","எட்டு",
				"ஒன்பது","பத்து","பதினொன்று","பன்னிரண்டு","பதின்மூன்று ","பதினான்கு","பதினைந்து","பதினாறு",
				"பதினேழு","பதினெட்டு","பத்தொன்பது","இருபது"
				});
		Multimap0.put("6",new Object[]{R.raw.one_numbers,R.raw.two_numbers,R.raw.three_numbers,R.raw.four_numbers,R.raw.five_numbers,
				R.raw.six_numbers,R.raw.seven_numbers,R.raw.eight_numbers,R.raw.nine_numbers,R.raw.ten_numbers,
				R.raw.eleven_numbers,R.raw.twelve_numbers,R.raw.thirteen_numbers,R.raw.fourteen_numbers,
				R.raw.fifteen_numbers,R.raw.sixteen_numbers,R.raw.seventeen_numbers,R.raw.eighteen_numbers,
				R.raw.nineteen_numbers,R.raw.twenty_numbers});
	   
	   
		
		ArrayList<HashMap> ReturnHashMap=new ArrayList<HashMap>(Arrays.asList(Multimap0,Multimap1,Multimap2));
	return ReturnHashMap;
	   
   }
}
