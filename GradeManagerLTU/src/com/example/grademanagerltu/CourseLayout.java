package com.example.grademanagerltu;



import java.text.DateFormat;
import java.util.ArrayList;






import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;













import android.R.color;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.appcompat.R.integer;
import android.app.ActionBar.LayoutParams;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CourseLayout extends ActionBarActivity {
	RelativeLayout Linear1;
	LinearLayout Linear2;
	LinearLayout Linear3;
	Listadapter lstAdapter;
	TermAdapterLoad TermAdapter;
	Button CourseAddIcon;
	Calendar calender=Calendar.getInstance();
	ListView lv,TermList;
	TextView GradeCourseColor,GradeTerm,GradeOccurrences;
	ImageView AddnoCourseIcon;
	DateFormat formate;
	Integer InsertOrUpdate=0,GetListPosition,CourseclickCount=0,count;
	ImageView NocourseIcon;
	//Integer CourseID;
	String SelectLayout="Course";
	String[] TxtCourseName,TxtSection,TxtType;
	SQLiteDatabase db;
	ArrayList<Integer> CourseID = new ArrayList<Integer>();
	ArrayList<String> CourseName = new ArrayList<String>();
	ArrayList<String> CourseSection = new ArrayList<String>();
	ArrayList<String> CourseType = new ArrayList<String>();
	HashMap<Integer,ArrayList<String>> Multimap;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_course_layout);
		formate=DateFormat.getDateInstance();
		CourseAddIcon=(Button)findViewById(R.id.CourseAddIcon);
		AddnoCourseIcon=new ImageView(this);
		GradeCourseColor=(TextView)findViewById(R.id.GradeCourse);
		GradeTerm=(TextView)findViewById(R.id.GradeTerm);
		GradeOccurrences=(TextView)findViewById(R.id.GradeOccurrences);
		Linear1=(RelativeLayout)findViewById(R.id.Linear1);
		Linear2=(LinearLayout)findViewById(R.id.Linear2);
		Linear3=(LinearLayout)findViewById(R.id.Linear3);
		NocourseIcon=(ImageView)findViewById(R.id.grade_nocourse);
		GradeCourseColor.setBackgroundColor(Color.parseColor("#A4A4A4"));
		AddnoCourseIcon.setBackgroundResource(R.drawable.grade_nocourse);
		Multimap=new HashMap<Integer,ArrayList<String>>();
			CourseLoad();
			
			
		
	//------------Grade Course Listener---------------------------------
				GradeCourseColor.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						SelectLayout="Course";
						Linear3.removeAllViews();
						Handler courseHandler=new Handler();
						courseHandler.postDelayed(new Runnable(){

							@Override
							public void run() {
								GradeCourseColor.setBackgroundColor(Color.parseColor("#A4A4A4"));
								//lstAdapter.notifyDataSetChanged();
								if(CourseclickCount!=0 && count!=0)
									Linear3.addView(lv);
								else{
									ImageView AddnoCourseIcon=new ImageView(CourseLayout.this);
									AddnoCourseIcon.setBackgroundResource(R.drawable.grade_nocourse1);
									LinearLayout.LayoutParams vp = 
									        new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, 
									                        LayoutParams.WRAP_CONTENT);
									AddnoCourseIcon.setLayoutParams(vp);
									//Linear3.removeAllViews();
									Linear3.addView(AddnoCourseIcon);
								}
									CourseclickCount=0;
									GradeTerm.setBackgroundColor(Color.parseColor("#BDBDBD"));
							}						
						},300);
						GradeCourseColor.getBackground().setAlpha(500);
					}
				});
				
	//------------ListOnselectedItemClick Listnear---------------
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				InsertOrUpdate=1;
				Context context;
				context=CourseLayout.this;
				LayoutInflater inflator=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
				View row=inflator.inflate(R.layout.course_listview,null);
				TextView CourseName=(TextView)row.findViewById(R.id.CourseName);
				//String IDs=CourseName.getTag().toString();
				//int ID=Integer.parseInt((String) CourseName.getTag());
				int ID=CourseID.get(arg2);
				Intent intent=new Intent(CourseLayout.this,AddCourses.class);
				intent.putExtra("ID",ID);
				intent.putExtra("InsertOrUpdate", InsertOrUpdate);
				startActivity(intent);
				finish();
			}
		});
		
	//------------------ListOnLongClickListener------------------------
		
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				GetListPosition=arg2;
				AlertDialog.Builder DeleteDialogBox = new AlertDialog.Builder(CourseLayout.this);
				DeleteDialogBox.setTitle("Confirm");
				DeleteDialogBox.setMessage("Are you sure to delete?");
				DeleteDialogBox.setPositiveButton("NO", new 
					    DialogInterface.OnClickListener() {
					        public void onClick(DialogInterface dialog, int which) {
					        	dialog.dismiss();
					            }
					        });
				DeleteDialogBox.setNegativeButton("YES", new 
					    DialogInterface.OnClickListener() {
					        public void onClick(DialogInterface dialog, int which) {
					        	db=openOrCreateDatabase("MyDb", MODE_PRIVATE, null);
								db.execSQL("DELETE FROM Courses WHERE CourseId='"+CourseID.get(GetListPosition)+"'");
								finish();
								startActivity(getIntent());
					            }
					        });
				DeleteDialogBox.setCancelable(true);
				//Dialog DeleteDialog=new Dialog(CourseLayout.this);
				//DeleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				DeleteDialogBox.show();
				/*db=openOrCreateDatabase("MyDb", MODE_PRIVATE, null);
				db.execSQL("DELETE FROM Courses WHERE CourseId='"+CourseID.get(arg2)+"'");
				finish();
				startActivity(getIntent());*/
				return true;
			}
		});
	//----------------------------------------------------------------------------
		
		Gradian(30, Linear1,"#D8D8D8");
		//Gradian(15, Linear3,"#FFFFFF");	
		//GradianTextbox(55, GradeCourseColor);
		
	//------------Course Add IconListeners-------------------------------------------------------
		CourseAddIcon.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//Toast.makeText(getApplicationContext(), "Hello", Toast.LENGTH_SHORT).show();
				Handler Colorcahnge=new Handler();
				Colorcahnge.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						//Toast.makeText(getApplicationContext(), "Hello", Toast.LENGTH_SHORT).show();
						if(SelectLayout.equals("Course")){
						CourseAddIcon.getBackground().setAlpha(500);
						Intent intent=new Intent(CourseLayout.this,AddCourses.class);
						intent.putExtra("ID",0);
						startActivity(intent);
						}
						else if(SelectLayout.equals("Term")){
							Intent intent=new Intent(CourseLayout.this,AddTerm.class);
							intent.putExtra("CourseName",CourseName);
							intent.putExtra("CourseID", CourseID);
							intent.putExtra("update", "notupdate");
							startActivity(intent);
						}
						else if(SelectLayout.equals("Attendence")){
							Intent intent=new Intent(CourseLayout.this,AddAttendence.class);
							startActivity(intent);
						}
					}
				},300);
				CourseAddIcon.getBackground().setAlpha(100);	
			}
		});
		
//------------Term Course Listener----------------------------------------------
		GradeTerm.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SelectLayout="Term";
				Handler handler=new Handler();
				handler.postDelayed(new Runnable(){

					@Override
					public void run() {	
						GradeTerm.setBackgroundColor(Color.parseColor("#A4A4A4"));
						GradeCourseColor.setBackgroundColor(Color.parseColor("#BDBDBD"));
						GradeOccurrences.setBackgroundColor(Color.parseColor("#BDBDBD"));
						CourseclickCount=1;
						db=openOrCreateDatabase("MyDb", MODE_PRIVATE, null);
						Cursor c=db.rawQuery("SELECT * FROM courses", null);
						int count=c.getCount();
						if(count!=0){	
						c.moveToFirst();
						for(int loop=1;loop<=count;loop++){
							//Multimap.put(loop-1, new ArrayList<String>(Arrays.asList(c.getString(c.getColumnIndex("CourseId")));
							 Multimap.put(loop-1, new ArrayList<String>(Arrays.asList(c.getString(c.getColumnIndex("CourseId")),
											 c.getString(c.getColumnIndex("CourseName")),
											 c.getString(c.getColumnIndex("StartDate")),
											 c.getString(c.getColumnIndex("EndDate")))));
							 c.moveToNext();
							}
						TermLoad();
						}
						else{
							Linear3.removeAllViews();
							ImageView NoTerm=new ImageView(CourseLayout.this);
							NoTerm.setBackgroundResource(R.drawable.grade_termicon);
							Linear3.addView(NoTerm);
						}
							
					}
				},300);
				GradeTerm.getBackground().setAlpha(0);
			}
		});
		GradeOccurrences.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Handler handler=new Handler();
				handler.postDelayed(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						SelectLayout="Attendence";
						Linear3.removeAllViews();
						GradeTerm.setBackgroundColor(Color.parseColor("#BDBDBD"));
						GradeCourseColor.setBackgroundColor(Color.parseColor("#BDBDBD"));
						GradeOccurrences.setBackgroundColor(Color.parseColor("#A4A4A4"));
					}
					
				},300);
				
				
			}
		});
	}
	
//-----------Course Loading Function----------------------------------------------
	void CourseLoad(){
		lv=new ListView(this);
		lstAdapter=new Listadapter(CourseLayout.this,CourseID,CourseName,CourseSection,CourseType);
		lv.setAdapter(lstAdapter);
		//lv.setDivider(sage);
			
	//----------Select Layout Function Call---------------------------------------
				if(SelectLayout.equals("Course")){
					//GradeCourseColor.getBackground().setAlpha(100);
					LoadLayout(SelectLayout );
				}
	}
	
//---------------Term Loading Function---------------------------------------------
	void TermLoad(){
		Linear3.removeAllViews();
		TermList=new ListView(this);
		TermAdapter=new TermAdapterLoad(CourseLayout.this, Multimap);
		TermList.setAdapter(TermAdapter);
		Linear3.addView(TermList);
		TermList.setOnItemClickListener(new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			String Sdate=Multimap.get(arg2).get(2);
			String Edate=Multimap.get(arg2).get(3);
			Intent intent=new Intent(CourseLayout.this,AddTerm.class);
			intent.putExtra("Droptext", arg2);
			intent.putExtra("CourseName",CourseName);
			intent.putExtra("CourseID", CourseID);
			intent.putExtra("Sdate", Sdate);
			intent.putExtra("Edate", Edate);
			intent.putExtra("update", "update");
			startActivity(intent);
		}
	});
	}

//-----------DataBase & Select Layout Function------------------------------------
	 void LoadLayout(String SelectLayout){
		 if(SelectLayout.equals("Course")){
		 db=openOrCreateDatabase("MyDb", MODE_PRIVATE, null);
			db.execSQL("CREATE TABLE IF NOT EXISTS COURSES(CourseId int NOT NULL, CourseName VARCHAR,CRN int,CourseSubject VARCHAR,CourseSection int, Level VARCHAR,Type VARCHAR,StartDate varchar,EndDate varchar,PRIMARY KEY(CourseId));");
			Cursor c=db.rawQuery("SELECT * FROM COURSES", null);
			count=c.getCount();
			c.moveToFirst();
			for(int DataFieldCnt=0;DataFieldCnt<count;DataFieldCnt++){
				CourseID.add(Integer.parseInt(c.getString(c.getColumnIndex("CourseId"))));
				CourseName.add(c.getString(c.getColumnIndex("CourseName")));
				CourseSection.add(c.getString(c.getColumnIndex("CourseSection")));
				CourseType.add(c.getString(c.getColumnIndex("Type")));
				c.moveToNext();
				
			}
			if(count!=0){
				Linear3.removeAllViews();
				Linear3.addView(lv);
			}
			else{
				
			}
		 }
	 }
//--------------------------------------------------------------------------------	
void Gradian(int num,RelativeLayout name,String color)
{
	GradientDrawable gd = new GradientDrawable();
    gd.setColor(Color.parseColor(color));
    gd.setCornerRadius(num);
    name.setBackgroundDrawable(gd);
}

void GradianTextbox(int num,TextView name)
{
	GradientDrawable gd = new GradientDrawable();
    gd.setCornerRadius(num);
    name.setBackgroundDrawable(gd);
}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.course_layout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}

//----------------BaseAdapater---------------------------------------------------
class Listadapter extends BaseAdapter{
	Context context;
	ArrayList<Integer> CourseID1=new ArrayList<Integer>();
	ArrayList<String> CourseName1=new ArrayList<String>();
	ArrayList<String> CourseSection1;
	ArrayList<String> CourseType1;
	//String[] Hint;
		public Listadapter(Context c,ArrayList<Integer> CourseID, ArrayList<String> CourseName,ArrayList<String> CourseSection,ArrayList<String> CourseType) {
			//super(c, R.layout.activity_main);
			this.context=c;
			this.CourseID1=CourseID;
			this.CourseName1=CourseName;
			this.CourseSection1=CourseSection;
			this.CourseType1=CourseType;
			//this.Hint=Hint;
			// TODO Auto-generated constructor stub
		}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		
		return CourseName1.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		final HashMap<Integer, Integer> map= new HashMap<Integer, Integer>();
		LayoutInflater inflator=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
		View row=inflator.inflate(R.layout.course_listview,arg2,false);
		TextView CourseName=(TextView)row.findViewById(R.id.CourseName);
		TextView CourseSection=(TextView)row.findViewById(R.id.CourseSection);
		TextView CourseType=(TextView)row.findViewById(R.id.Type);
		CourseName.setText(CourseName1.get(arg0));
		CourseSection.setText(CourseSection1.get(arg0));
		CourseType.setText(CourseType1.get(arg0));
		CourseName.setTag(CourseID1.get(arg0));
		//map.put(arg0, CourseID1.get(arg0));
		return row;
	}
}

class TermAdapterLoad extends BaseAdapter{
	Context context;
	HashMap<Integer,ArrayList<String>> Multimap;
	public TermAdapterLoad(Context context,HashMap<Integer,ArrayList<String>> Multimap){
		this.Multimap=Multimap;
		this.context=context;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return Multimap.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		LayoutInflater inflator=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
		View row=inflator.inflate(R.layout.term_listview,arg2,false);
		TextView TermCourse=(TextView)row.findViewById(R.id.TXTTermCourseName);
		TextView TermStart=(TextView)row.findViewById(R.id.TXTTermStart);
		TextView TermEnd=(TextView)row.findViewById(R.id.TXTTermEnd);
		TermCourse.setText(Multimap.get(arg0).get(1));
		TermStart.setText(Multimap.get(arg0).get(2));
		TermEnd.setText(Multimap.get(arg0).get(3));
		return row;
	}
	
}
//-------------------------------------------------------------	
