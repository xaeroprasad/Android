package com.example.grademanagerltu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.larswerkman.holocolorpicker.*;

public class AddAttendence extends ActionBarActivity {
	Button AttendencePresent,AttendenceLate,AttendenceAbsent,gradeWeekdaysIcon;
	RelativeLayout AttendenceLinear2;
	ListView Multiselect;
	TextView textView4;
	AlertDialog.Builder AddlistDialog;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_attendences);
		AttendencePresent=(Button)findViewById(R.id.AttendencePresent);
		AttendenceLate=(Button)findViewById(R.id.AttendenceLate);
		AttendenceAbsent=(Button)findViewById(R.id.AttendenceAbsent);
		gradeWeekdaysIcon=(Button)findViewById(R.id.gradeWeekdaysIcon);
		AttendenceLinear2=(RelativeLayout)findViewById(R.id.AttendenceLinear2);
		AttendencePresent.setBackgroundColor(Color.GREEN);
		AttendenceLate.setBackgroundColor(Color.YELLOW);
		AttendenceAbsent.setBackgroundColor(Color.RED);
		textView4=(TextView)findViewById(R.id.textView4);
		Gradian(15,AttendencePresent,"#01DF01");
		Gradian(15,AttendenceLate,"#FFFF00");
		Gradian(15,AttendenceAbsent,"#FF0000");
		final String List[]={"Mon","Tue","Wed","Thu","Fri","Sat","Sun"};
		Multiselect=new ListView(this);
		ArrayAdapter<String> AD=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_multiple_choice,List);
		Multiselect.setAdapter(AD);
		Multiselect.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		
		gradeWeekdaysIcon.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Handler handler=new Handler();
				handler.postDelayed(new Runnable(){

					@Override
					public void run() {
						AddlistDialog = new AlertDialog.Builder(AddAttendence.this);
						AddlistDialog.setView(Multiselect);
						gradeWeekdaysIcon.getBackground().setAlpha(500);
						
						AddlistDialog.setPositiveButton("Cancel", new 
					    DialogInterface.OnClickListener() {
					        public void onClick(DialogInterface dialog, int which) {
					        	dialog.dismiss();
					        	dialog.cancel(); 	
					        	Intent intent=new Intent(AddAttendence.this,AddAttendence.class);
					        	startActivity(intent);
			
					            }
					        });
						AddlistDialog.setNegativeButton("Ok", new 
							    DialogInterface.OnClickListener() {
							        public void onClick(DialogInterface dialog, int which) {
							        	
							        	SparseBooleanArray sba=Multiselect.getCheckedItemPositions();
							        	String Concat="";
							        	for(int count=0;count<sba.size();count++){
							        		if(count!=sba.size()-1)
							        		 Concat=Concat+List[sba.keyAt(count)]+",";
							        		else
							        			Concat=Concat+List[sba.keyAt(count)];
							        	}
							        	textView4.setText(Concat);
							        	Toast.makeText(getApplicationContext(), Concat, Toast.LENGTH_SHORT).show();
							        	Intent intent=new Intent(AddAttendence.this,AddAttendence.class);
							        	startActivity(intent);
							            }
							        });
						AddlistDialog.show();
					}
					
				},300);
				gradeWeekdaysIcon.getBackground().setAlpha(100);			
			}
		});
	}
	void Gradian(int num,Button name,String color)
	{
		GradientDrawable gd = new GradientDrawable();
	    gd.setColor(Color.parseColor(color));
	    gd.setCornerRadius(num);
	    name.setBackgroundDrawable(gd);
	}
}
