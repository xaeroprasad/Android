package com.example.grademanagerltu;

import java.net.DatagramPacket;
import java.text.DateFormat;
import java.text.Format;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.R.color;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.appcompat.R.integer;
import android.app.ActionBar.LayoutParams;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AddTerm extends ActionBarActivity {
	SQLiteDatabase db;
	String update="new";
	Button TermStartDate1,TermEndDate,TermRightIcon;
	RelativeLayout TermLinear2;
	Integer value;
	TextView TXTStartDate,TXTEndDate;
	Format formate;
	ArrayList<String> CourseName=new ArrayList<String>();
	ArrayList<Integer> CourseID=new ArrayList<Integer>();
	Spinner dropdownList;
	Calendar calender=Calendar.getInstance();
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		formate=DateFormat.getDateInstance();
		setContentView(R.layout.add_new_term);
		TermLinear2=(RelativeLayout)findViewById(R.id.TermLinear2);
		TermRightIcon=(Button)findViewById(R.id.TermRightIcon);
		TermEndDate=(Button)findViewById(R.id.TermEndDate1);
		TXTStartDate=(TextView)findViewById(R.id.TXTStartDate);
		TXTEndDate=(TextView)findViewById(R.id.TXTEndDate);
		TermStartDate1=(Button)findViewById(R.id.TermStartDate1);
		dropdownList = (Spinner)findViewById(R.id.DropdownList);
		Bundle SelectedItemextra=getIntent().getExtras();
		update=SelectedItemextra.getString("update");
		
//-------------------Populate Spinner Items-----------------------------------------
					
				Bundle extra=getIntent().getExtras();
				CourseName=extra.getStringArrayList("CourseName");
				CourseID=extra.getIntegerArrayList("CourseID");
				//Enterprise IT
				//int a=CourseID.indexOf("Esl");
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, CourseName);
				dropdownList.setAdapter(adapter);
				if(update.equals("update")){
					int dropdown=SelectedItemextra.getInt("Droptext");
					String Sdate=SelectedItemextra.getString("Sdate");
					String Edate=SelectedItemextra.getString("Edate");
					
					dropdownList.setSelection(dropdown);
					TXTStartDate.setText(Sdate);
					TXTEndDate.setText(Edate);
				}

//-------------------Term Right Icon Listener---------------------------------------
			
				TermRightIcon.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Handler handler=new Handler();
						handler.postDelayed(new Runnable(){

							@Override
							public void run() {
								TermRightIcon.getBackground().setAlpha(500);
								String startDate=TXTStartDate.getText().toString();
								String endDate=TXTEndDate.getText().toString();
								if(TXTStartDate.getText().toString().equals("")|| TXTEndDate.getText().toString().equals("")){
									Toast.makeText(getApplicationContext(), "Please Fill all entries", Toast.LENGTH_SHORT).show();
								}
								else{
									String dropdownSelectedItem= dropdownList.getSelectedItem().toString();
									for(int loop=0;loop<CourseName.size();loop++){
										String a=dropdownSelectedItem;
										if(CourseName.get(loop).equals(dropdownSelectedItem)){
											int ID=CourseID.get(loop);
											db=openOrCreateDatabase("MyDb", MODE_PRIVATE, null);
											db.execSQL("UPDATE courses SET StartDate='"+startDate+"', EndDate='"+endDate+"' WHERE CourseId='"+ID+"'");
											Toast.makeText(getApplicationContext(), "Successful", Toast.LENGTH_SHORT).show();
											Intent intent=new Intent(AddTerm.this,CourseLayout.class);
											startActivity(intent);
											finish();
										}
									}
								}
								
							}
							
						},300);	
						TermRightIcon.getBackground().setAlpha(100);
					}
				});
//-------------------Start Term Date Listener---------------------------------------
		TermStartDate1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Handler handler=new Handler();
				handler.postDelayed(new Runnable(){

					@Override
					public void run() {
						TermStartDate1.getBackground().setAlpha(500);
						value=0;
						Calendar(value);
					}
					
				},300);
				TermStartDate1.getBackground().setAlpha(100);
				
			}
		});

//----------------End Term Date Listener---------------------------------------------
		TermEndDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Handler handler=new Handler();
				handler.postDelayed(new Runnable(){

					@Override
					public void run() {
						TermEndDate.getBackground().setAlpha(500);
						value=1;
						Calendar(value);
						
					}
					
				},300);
				TermEndDate.getBackground().setAlpha(100);
			}
		});
 }
	
//-------------------Calendar-------------------------------------------------------	
	public void Calendar(Integer Value){
		final Calendar c = Calendar.getInstance();
		Integer mYear = c.get(Calendar.YEAR);
		Integer mMonth = c.get(Calendar.MONTH);
		Integer mDay = c.get(Calendar.DAY_OF_MONTH);
		DatePickerDialog dpd = new DatePickerDialog(this,
		        new DatePickerDialog.OnDateSetListener() {
		 
		            @Override
		            public void onDateSet(DatePicker view, int year,
		                    int monthOfYear, int dayOfMonth) {
		            	if(value==0){
		            	TXTStartDate.setText((monthOfYear + 1) + "-"
		                        + dayOfMonth + "-" + year);
		            	}
		            	else{
		            		TXTEndDate.setText((monthOfYear + 1) + "-"
			                        + dayOfMonth + "-" + year);
		            	}
		            }
		        }, mYear, mMonth, mDay);
		dpd.show();
	}
}
