package com.example.grademanagerltu;



import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class AddCourses extends ActionBarActivity {
	SQLiteDatabase db;
	Button GradeCorrect,GradeCancel;
	TextView TXTCourseName,TXTCRN,TXTCourseSection,TXTLevel,TXTType,TXTCourseSubject;
	String CourseName,Level,Type,CourseSubject;
	Integer CRN,CourseId,CourseSection,IntentID,IntentInsertOrUpdate;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_new_course);
		GradeCorrect=(Button)findViewById(R.id.GradeRightIcon);
		GradeCancel=(Button)findViewById(R.id.GradeWrongIcon);
		TXTCourseName=(TextView)findViewById(R.id.TXTCourse);
		TXTCRN=(TextView)findViewById(R.id.TXTCRN);
		TXTCourseSubject=(TextView)findViewById(R.id.TXTSubject);
		TXTCourseSection=(TextView)findViewById(R.id.TXTCourseSection);
		TXTLevel=(TextView)findViewById(R.id.TXTLevel);
		TXTType=(TextView)findViewById(R.id.TXTType);
		Bundle extra=getIntent().getExtras();
		IntentID=extra.getInt("ID");
		IntentInsertOrUpdate=extra.getInt("InsertOrUpdate");
		if(IntentInsertOrUpdate==1){
			db=openOrCreateDatabase("MyDb", MODE_PRIVATE, null);
			Cursor c=db.rawQuery("select * from courses where CourseId='"+IntentID+"'",null);
			c.moveToFirst();
			TXTCourseName.setText(c.getString(c.getColumnIndex("CourseName")));
			TXTCRN.setText(c.getString(c.getColumnIndex("CRN")));
			TXTCourseSubject.setText(c.getString(c.getColumnIndex("CourseSubject")));
			TXTCourseSection.setText(c.getString(c.getColumnIndex("CourseSection")));
			TXTLevel.setText(c.getString(c.getColumnIndex("Level")));
			TXTType.setText(c.getString(c.getColumnIndex("Type")));
		}
	//--------------------RightButton Icon Listener--------------------------------------------
		GradeCorrect.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				CourseName=TXTCourseName.getText().toString();
				String CRNTemp=TXTCRN.getText().toString();
				CourseSubject=TXTCourseSubject.getText().toString();
				String CourseSectionTemp=TXTCourseSection.getText().toString();
				Level=TXTLevel.getText().toString();
				Type=TXTType.getText().toString();
				if(CourseName.equals("") || Level.equals("") || Type.equals("") || CourseSectionTemp.equals("") || CRNTemp.equals("") ){
					Toast.makeText(getApplicationContext(), "Please fill all entries ", Toast.LENGTH_SHORT).show();
				}
				else{
					CRN=Integer.parseInt(CRNTemp);
					CourseSection=Integer.parseInt(CourseSectionTemp);
				CourseId=Integer.parseInt(CRNTemp+CourseSectionTemp);
				db=openOrCreateDatabase("MyDb", MODE_PRIVATE, null);
				db.execSQL("CREATE TABLE IF NOT EXISTS COURSES(CourseId int NOT NULL, CourseName VARCHAR,CRN int,CourseSubject VARCHAR,CourseSection int, Level VARCHAR,Type VARCHAR,StartDate varchar,EndDate varchar,PRIMARY KEY(CourseId));");
				if(IntentInsertOrUpdate==0){
					Handler RightIconColorChange=new Handler();
					RightIconColorChange.postDelayed(new Runnable() {	
						@Override
						public void run() {
							GradeCorrect.getBackground().setAlpha(500);
						}
					},300);
					GradeCorrect.getBackground().setAlpha(100);
				db.execSQL("INSERT INTO COURSES VALUES('"+CourseId+"','"+CourseName+"','"+CRN+"','"+CourseSubject+"','"+CourseSection+"','"+Level+"','"+Type+"','"+"NULL"+"','"+"NULL"+"');");
				}
				else{
					Handler RightIconColorChange1=new Handler();
					RightIconColorChange1.postDelayed(new Runnable() {	
						@Override
						public void run() {
							GradeCorrect.getBackground().setAlpha(500);
						}
					},300);
					GradeCorrect.getBackground().setAlpha(100);
					db.execSQL("UPDATE courses SET CourseName='"+CourseName+"', CRN='"+CRN+"',CourseSubject='"+CourseSubject+"',CourseSection='"+CourseSection+"',Level='"+Level+"',Type='"+Type+"' WHERE CourseId='"+IntentID+"'");
				}
				Toast.makeText(getApplicationContext(), "Sucessful", Toast.LENGTH_SHORT).show();
				Intent intent=new Intent(AddCourses.this,CourseLayout.class);
				startActivity(intent);
				finish();
				}
			}
		});
	//------------------------------------------------------------------------------------
	}
}
