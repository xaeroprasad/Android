package com.example.androidpermissionmanager;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
















import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.DocumentsContract.Document;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ViewAppPermissions extends ActionBarActivity {
	ListView List_Permissions;
	TextView TxtCatagoryName,TxtPaidOrFree,TxtSubcatName,TxtSubcatName2;
	int SelectedListItemNo;
	int ElementExists=0,CatagoryExist=0;
	String PackageName,PaidorFree,AppCatagoryName,SubcatName,CpyAppPurchase,CpyAddSupport;
	String connectionURL="Sucess";
	org.jsoup.nodes.Document html = null;
	org.jsoup.nodes.Document htmlExtract = null;
	NetworkInfo activeNetwork ;
	ArrayList<String> ThirdPtyPackName=new ArrayList<String>();
	ArrayList<String> keys=new ArrayList<String>();
	ArrayList<String> AddCheckCatagoryList=new ArrayList<String>();
	ArrayList<String> myKey=new ArrayList<String>();
	ArrayList<String> addallArrayList=new ArrayList<String>();
	ArrayList PermissionList=new ArrayList();
	ArrayList PermissionListCopy=new ArrayList();
	ArrayList NopermissionMatchList=new ArrayList();
	ArrayList<String> GetListFromHashMap=new ArrayList<String>();
	HashMap<Integer,ArrayList<String>> Multimap=new HashMap<Integer,ArrayList<String>>();	
	HashMap<String,ArrayList<String>> GetAndPerMap=new HashMap<String,ArrayList<String>>();	
	LinkedHashMap<String,ArrayList<String>> GetAndPerMap1=new LinkedHashMap<String,ArrayList<String>>();	
	HashMap<String,ArrayList<String>> AndroidPermissionMultimap=new HashMap<String,ArrayList<String>>();	
	HashMap<String ,HashMap<String ,ArrayList<String>>> HashInHash=new HashMap<String, HashMap<String ,ArrayList<String>>>();
	LinkedHashMap<String,ArrayList<String>> sortedMap=new LinkedHashMap<String,ArrayList<String>>();	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_app_permission);
		List_Permissions=(ListView)findViewById(R.id.List_Permissions);
		List_Permissions.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);   
		List_Permissions.setItemsCanFocus(false);
		TxtCatagoryName=(TextView)findViewById(R.id.CatagoryName);
		TxtSubcatName2=(TextView)findViewById(R.id.TxtSubcatName2);
		TxtSubcatName=(TextView)findViewById(R.id.TxtSubcatName);
		TxtPaidOrFree=(TextView)findViewById(R.id.PaidOrFree);
		Bundle bundle = this.getIntent().getExtras();
		Multimap=(HashMap<Integer, ArrayList<String>>) bundle.getSerializable("HashMap");
		SelectedListItemNo=bundle.getInt("SelectedListItemNo");
		ThirdPtyPackName=bundle.getStringArrayList("ThirdPtyPackName");
		PackageName=ThirdPtyPackName.get(SelectedListItemNo);
		AndroidPermissionCollection GetListData=new AndroidPermissionCollection();
		
		AndroidPermissionMultimap=GetListData.AndroidpermissionsCollectionList();
		
		
//------------------------------Listing permission in order for each application---------------------------		
		for(int count=0;count<Multimap.get(SelectedListItemNo).size();count++){
			PermissionListCopy.add(Multimap.get(SelectedListItemNo).get(count));
		}		
//-----------------------------Selecting permission from Android permission class----------------------------
		for(int count=0;count<PermissionListCopy.size();count++){
			if(AndroidPermissionMultimap.get(PermissionListCopy.get(count))!=null){
				GetAndPerMap.put((String) PermissionListCopy.get(count), AndroidPermissionMultimap.get(PermissionListCopy.get(count)));		
			     }else{
			    	 NopermissionMatchList.add(PermissionListCopy.get(count));
			     }
			}
//---------------------------------------------------------------------------------------------------------------		
		PermissionList.addAll(GetAndPerMap.keySet());
//--------------------------------------Sorting permissions------------------------------------------------------
		if(GetAndPerMap.size()!=0){
		for(int count=0;count<PermissionList.size();count++){
			if(count==0){
				sortedMap.put((String) PermissionList.get(count), GetAndPerMap.get(PermissionList.get(count)));
				addallArrayList.addAll(GetAndPerMap.get(PermissionList.get(count)));
				AddCheckCatagoryList.add(addallArrayList.get(0));
				Log.d("ff", addallArrayList.get(0));
				Log.d("ff", addallArrayList.get(1));
				Log.d("ff", addallArrayList.get(2));
				addallArrayList.clear();
			}else{
				for(int check=0;check<AddCheckCatagoryList.size();check++){
					if(GetAndPerMap.get(PermissionList.get(count)).get(0).equals(AddCheckCatagoryList.get(check))){
						CatagoryExist=1;
					}
				} if(CatagoryExist!=1){
					addallArrayList.clear();
					sortedMap.put((String) PermissionList.get(count), GetAndPerMap.get(PermissionList.get(count)));
					AddCheckCatagoryList.add(GetAndPerMap.get(PermissionList.get(count)).get(0));
				         }
					if(CatagoryExist!=1){
						//sortedMap.put((String) PermissionList.get(count), GetAndPerMap.get(PermissionList.get(count)));
						addallArrayList.addAll(GetAndPerMap.get(PermissionList.get(count)));
						//AddCheckCatagoryList.add(addallArrayList.get(0));
						Log.d("ff", addallArrayList.get(0));
						Log.d("ff", addallArrayList.get(1));
						Log.d("ff", addallArrayList.get(2));
						addallArrayList.clear();
					}
				
			}
		 if(CatagoryExist!=1){	
		 for(int count2=count+1;count2<PermissionList.size();count2++){
				if(GetAndPerMap.get(PermissionList.get(count)).get(0).equals(GetAndPerMap.get(PermissionList.get(count2)).get(0))){
					sortedMap.put((String) PermissionList.get(count2), GetAndPerMap.get(PermissionList.get(count2)));
					addallArrayList.addAll(GetAndPerMap.get(PermissionList.get(count2)));
				}
			}	
		}
		 CatagoryExist=0;
			Log.d("end", "----------------------");
		  } 
		myKey.addAll(sortedMap.keySet());
	}
		else{
			sortedMap.put("0", NopermissionMatchList);
		}
		
		
		Listadapter lstadapter1=new Listadapter(ViewAppPermissions.this,sortedMap,myKey);
		List_Permissions.setAdapter(lstadapter1);
		DummyAsyncTask task=new DummyAsyncTask();
		task.execute();
	
	}
	
	
	class Listadapter extends BaseAdapter{
		Context context;
		ArrayList<String>myKey;
		LinkedHashMap<String,ArrayList<String>> sortedMap;	
		//ArrayList PermissionList;
			public Listadapter(Context context,LinkedHashMap sortedMap,ArrayList<String>myKey){
				this.context=context;
				this.sortedMap=sortedMap;
				this.myKey=myKey;
			}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return sortedMap.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			LayoutInflater inflator=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			View row=inflator.inflate(R.layout.list_permission_mapping,arg2,false);
			TextView CatagoryTitle=(TextView)row.findViewById(R.id.CatagoryTitle);
			TextView CatagoryName=(TextView)row.findViewById(R.id.CatagoryName);
			TextView CatagoryDetails=(TextView)row.findViewById(R.id.CatagoryDetail);
			TextView PermissionName=(TextView)row.findViewById(R.id.PermissionName);
			View viewname=(View)row.findViewById(R.id.view1);
			String DangerousLevel=sortedMap.get(myKey.get(arg0)).get(3);
			if(arg0!=0){
				String CopyCatagoryName=sortedMap.get(myKey.get(arg0-1)).get(0);
				if(CopyCatagoryName.equals(sortedMap.get(myKey.get(arg0)).get(0))){
					CatagoryTitle.setText("");
					((ViewGroup) viewname.getParent()).removeView(viewname);
				}else{
					CatagoryTitle.setText(sortedMap.get(myKey.get(arg0)).get(0));
				}
			}else{
				CatagoryTitle.setText(sortedMap.get(myKey.get(arg0)).get(0));
			}
			if(DangerousLevel.equalsIgnoreCase("1")){
				CatagoryName.setTextColor(Color.RED);
				PermissionName.setTextColor(Color.RED);
				CatagoryDetails.setTextColor(Color.RED);
			}
			CatagoryName.setText(myKey.get(arg0));
			PermissionName.setText(sortedMap.get(myKey.get(arg0)).get(1));
			CatagoryDetails.setText(sortedMap.get(myKey.get(arg0)).get(2));
			return row;
		}
		
	}
	
	
	
	
	
	public class DummyAsyncTask extends AsyncTask<String, Void, Void>
	{
		private ProgressDialog progressDialog = new ProgressDialog(ViewAppPermissions.this);
	    InputStream is = null ;
	    String result = "";
	    protected void onPreExecute() {
	       progressDialog.setMessage("Fetching data...");
	       progressDialog.show();
	       }
	
		@Override
		protected Void doInBackground(String... params) {
			try{
				Context context;
				  ConnectivityManager cm = (ConnectivityManager) getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);
		    activeNetwork = cm.getActiveNetworkInfo();
			}catch(Exception e){
				Toast.makeText(getApplicationContext(), "Check Internet connection", Toast.LENGTH_SHORT).show();
			}
			//String url="http://androidpermission.comuv.com/PermissionList.php";	
			  String url = "https://play.google.com/store/apps/details?id="+PackageName+"&hl=en";
			  String url_selec1 = "http://hmkcode.appspot.com/rest/controller/get.json";
			  InputStream inputStream = null;
		        String result = "";
		        try {
		 
		            // create HttpClient
		            HttpClient httpclient = new DefaultHttpClient();
		            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
		            inputStream = httpResponse.getEntity().getContent();
					try {
					    html = Jsoup.connect(url).get();
					} catch (final Exception e) {
					    Log.d("error", e.toString());
					    TxtCatagoryName.setText("App Not found in Google Play Store");
					    connectionURL="Failed";
					    return null;
					}
		            if(inputStream != null){
		            	 BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
		            //	BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
		            String line = "";
		            while((line = bufferedReader.readLine()) != null)
		                result += line;
		            inputStream.close();
		            }
		            else
		                result = "Did not work!";
		 
		        } catch (Exception e) {
		            Log.d("InputStream", e.getLocalizedMessage());
		            TxtCatagoryName.setText("App Not found in Google Play Store");
		            //TxtSubcatName2.setText("App Not found in Google Play Store");
				    connectionURL="Failed";
		        }
					return null;

				}
		protected void onPostExecute(Void v) {
			if(connectionURL.equalsIgnoreCase("Failed")){
				//TxtSubcatName2.setText("App Not found in Google Play Store");
				TxtCatagoryName.setText("App Not found in Google Play Store");		 
				this.progressDialog.dismiss();
			}else{
			TextView Txt1=(TextView)findViewById(R.id.textView1);
			progressDialog.cancel();
			//Txt1.setText(html.toString());
			
			/*Elements newsHeadlines1 = html.select("div.info-container").select("div.info-container").
					select("a").select(".document-subtitle").select("span[itemprop=\"genre\"]");*/
			
			Elements SubcatEle = html.select("div.info-container").select("div.info-container").
					select("a").select(".document-subtitle").select("span[itemprop=\"name\"]");
			Elements newsHeadlines = html.select("div.info-container").select("div.info-container").
					select("a").select(".document-subtitle").select("span[itemprop=\"genre\"]");
			Element doc=html.select("div.details-info").select("div.info-container").
					select("div.details-actions").select("span").select("button").select("span").last();
			// Elements elements = html.select("div.info-container").select("div.info-container > span.genre");
			Element newsHeadlines2 = html.select("div.info-container").select("div.info-container").
					select("a").select(".document-subtitle").select("span[itemprop=\"genre\"]").last();
			Elements AppPurchase = html.select("div.info-container").select("div.info-container")
					.select("div.inapp-msg");
			Elements AddSupport = html.select("div.info-container").select("div.info-container")
					.select("span.ads-supported-label-msg");
			org.jsoup.nodes.Element elemet = newsHeadlines.first();
			AppCatagoryName=elemet.ownText();
		    PaidorFree=doc.ownText();
		    SubcatName=SubcatEle.text();
		    String subcat2=newsHeadlines2.text();
		    CpyAppPurchase=AppPurchase.text();
		    CpyAddSupport=AddSupport.text();
				this.progressDialog.dismiss();	
				
				if(subcat2.equalsIgnoreCase("") || subcat2.equalsIgnoreCase(AppCatagoryName)){
					TxtSubcatName2.setText("");
				}else
					TxtSubcatName2.setText(subcat2);
					
				TxtCatagoryName.setText(AppCatagoryName.toString());
				
				if(SubcatName.equalsIgnoreCase("")){
					TxtSubcatName.setText("");
				}else{
					TxtSubcatName.setText(SubcatName);
					 String text2 = "<font color=#FF0000>(Has Adds)</font>";
					 	if(CpyAddSupport.equalsIgnoreCase("Ad-supported family app"))
						TxtSubcatName.append(Html.fromHtml(text2));
				}
				if(PaidorFree.equalsIgnoreCase("Install")){
					if(CpyAppPurchase.equalsIgnoreCase("Offers in-app purchases")){
					TxtPaidOrFree.setText("Free");
					TxtPaidOrFree.setTextColor(Color.RED);
					}else{
						TxtPaidOrFree.setText("Free");
					}
				}else
					TxtPaidOrFree.setText("Paid");
			}	
		}
	
	
	}
}


